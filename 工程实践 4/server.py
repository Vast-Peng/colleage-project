import pymongo
import uuid, datetime, socket, sys, hashlib

device_uuid = 'c5eef2ed-52e9-488a-b248-fe82cac41cf0'

device_data = {
    'uuid': str(uuid.uuid4()),
    'device_uuid': device_uuid,
    'value': 0,
    'time': datetime.datetime.now()
}

connection = pymongo.MongoClient('localhost', 27017)
db = connection.engineering
record = db.record

# 创建 socket 对象
serversocket = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM) 
            
# 获取本地主机名
host = socket.gethostname()

port = 9999

# 绑定端口
serversocket.bind((host, port))

# 设置最大连接数，超过后排队
serversocket.listen(5)

print('等待连接')

while True:
    # 建立客户端连接
    clientsocket,addr = serversocket.accept()      

    print("连接地址: %s" % str(addr))
   
    
    msg='插入成功'+ "\r\n"
    
    value = float((clientsocket.recv(1024)).decode('utf-8'))
    
    device_data['value'] = value
    record.insert_one(device_data)
   
    clientsocket.send(msg.encode('utf-8'))
    clientsocket.close()
    
    break


