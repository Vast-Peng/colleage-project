from django.shortcuts import render

# -*- coding: utf-8 -*-
import os
from django.shortcuts import render
from django.views.generic.base import View
from django.conf import settings


class DocIndexView(View):

    def get(self, request):
        BASE_DIR = settings.BASE_DIR
        DOC_DIR = getattr(settings, 'DOC_DIR', None)
        if not DOC_DIR:
            DOC_DIR = os.path.join(BASE_DIR, 'docs')

        filenames = [(filename, filename.split('.')[0]) for filename in os.listdir(DOC_DIR)]
        return render(
            request=request,
            template_name='swagger-index.html',
            context={'filenames': filenames},
        )


# Create your views here.
def login(request):
    return render(request, 'login.html')


def register(request):
    return render(request, 'register.html')


def index(request):
    return render(request, 'index.html')


def add_house(request):
    return render(request, "create_house.html")


def list_house(request):
    return render(request, "list_house.html")


def modify_house(request):
    return render(request, "modify_house.html")


def list_device(request):
    return render(request, "list_device.html")


def list_record(request):
    return render(request, "list_record.html")


def add_device(request):
    return render(request, "add_device.html")
