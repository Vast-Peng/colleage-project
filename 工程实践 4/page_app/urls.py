from django.urls import path
from . import views

urlpatterns = [
    path('login', views.login),
    path('register', views.register),
    path('index', views.index),
    path('add_house', views.add_house),
    path('list_house', views.list_house),
    path('modify_house', views.modify_house),
    path('list_device', views.list_device),
    path('list_record', views.list_record),
    path('add_device', views.add_device)
]
