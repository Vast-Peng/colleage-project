from .models import *
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class meta:
        model = User
        fields = ('uuid', 'nick_name', 'description')


class HouseSerializer(serializers.HyperlinkedModelSerializer):
    class meta:
        model = House
        fields = ('uuid', 'name', 'user_uuid', 'description', 'time')


class DeviceSerializer(serializers.HyperlinkedModelSerializer):
    class meta:
        model = Device
        fields = ('uuid', 'house_uuid', 'name', 'description', 'price', 'oem', 'time')


class RecordSerializer(serializers.HyperlinkedModelSerializer):
    class meta:
        model = Record
        fields = ('device_uuid', 'value', 'time')
