from django.core.cache import cache
from engineering.models import *
from engineering import Response
from engineering.utils import *
from engineering.config import *


def all_records(request):
    token = request.GET.get('token')
    device_uuid = request.GET.get('device_uuid')

    if token is None or device_uuid is None or token == '' or device_uuid == '':
        return Response.PARAMS_BIND_ERROR

    user_uuid = cache.get(token)
    if user_uuid is None:
        return Response.TOKEN_ERROR

    device = Device.objects.get(uuid=device_uuid)
    if device is None:
        return Response.DEVICE_NOT_EXIST

    house_uuid = device.house_uuid
    if House.objects.filter(Q(uuid=house_uuid) & Q(user_uuid=user_uuid)):
        records = Record.objects(device_uuid=device_uuid).order_by('-time')
        cache.expire(token, timeout=TOKEN_MAX_EXIST_TIME)
        return Response.list_records_of_device(records)
    else:
        return Response.NO_AUTH


def get_latest(request):
    device_uuid = request.GET.get('device_uuid')
    token = request.GET.get('token')

    if token is None or device_uuid is None or token == "" or device_uuid == "":
        return Response.PARAMS_BIND_ERROR

    user_uuid = cache.get('token')
    if user_uuid is None:
        return Response.TOKEN_ERROR

    device = Device.objects.get(uuid=device_uuid)
    if device is None:
        return Response.PARAMS_BIND_ERROR

    house_uuid = device.house_uuid
    house = House.objects.get(uuid=house_uuid, user_uuid=user_uuid)
    if house is None:
        return Response.NO_AUTH

    device_latest_value = Record.objects.get(device_uuid=device_uuid).order_by('-time')[0]

    cache.expire('token', timeout=TOKEN_MAX_EXIST_TIME)

    return Response.latest_record(device_latest_value)


def add_record(request):
    token = request.GET.get('token')
    device_uuid = request.GET.get('device_uuid')
    try:
        value = float(request.GET.get('value'))
    except ValueError:
        return Response.PARAMS_BIND_ERROR

    if token is None or device_uuid is None or token == '' or device_uuid == '':
        return Response.PARAMS_BIND_ERROR

    user_uuid = cache.get(token)
    if user_uuid is None:
        return Response.TOKEN_ERROR

    device = Device.objects.get(uuid=device_uuid)
    if device is None:
        return Response.DEVICE_NOT_EXIST

    house_uuid = device.house_uuid
    if House.objects.filter(Q(uuid=house_uuid) & Q(user_uuid=user_uuid)):
        record = Record(device_uuid=device_uuid, value=value)
        record.save()
        cache.expire(token, timeout=TOKEN_MAX_EXIST_TIME)
        return Response.OPERATION_SUCCESSFULLY
    else:
        return Response.NO_AUTH


def index(request):
    if request.method == 'GET':
        return all_records(request)
    elif request.method == 'POST':
        return add_record(request)
    else:
        return Response.METHOD_ERROR
