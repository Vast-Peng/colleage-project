from django.core.cache import cache
from engineering.models import *
from engineering import Response
from engineering.config import *
from rest_framework.views import APIView


def add_house(request):
    if request.is_ajax():
        name = request.POST.get('name')
        token = request.POST.get('token')
    else:
        name = request.GET.get('name')
        token = request.GET.get('token')

    # 参数校验
    if name == '' or token == '' or name is None or token is None:
        return Response.PARAMS_BIND_ERROR

    # 获取 uuid
    user_uuid = cache.get(token)
    if user_uuid is None:
        return Response.TOKEN_ERROR
    else:
        house = House(name=name, user_uuid=user_uuid)
        house.save()
        cache.expire(token, timeout=TOKEN_MAX_EXIST_TIME)
        return Response.OPERATION_SUCCESSFULLY


def delete_house(request):
    if not request.is_ajax():
        token = request.GET.get('token')
        house_uuid = request.GET.get('house_uuid')
    else:
        token = request.POST.get('token')
        house_uuid = request.POST.get('house_uuid')

    # 参数校验
    if token == '' or house_uuid == '' or token is None or house_uuid is None:
        return Response.PARAMS_BIND_ERROR

    # 获取用户 uuid
    user_uuid = cache.get(token)
    if user_uuid is None:
        return Response.TOKEN_ERROR
    else:
        house = House.objects.filter(uuid=house_uuid, user_uuid=user_uuid)
        if len(house) == 0:
            return Response.NO_AUTH
        else:
            cache.expire(token, timeout=TOKEN_MAX_EXIST_TIME)
            return Response.OPERATION_SUCCESSFULLY


def list_houses(request):
    token = request.GET.get('token')

    # 参数校验
    if token == '' or token is None:
        return Response.PARAMS_BIND_ERROR

    # 用户 uuid
    user_uuid = cache.get(token)
    if user_uuid is None:
        return Response.TOKEN_ERROR
    else:
        houses = House.objects(user_uuid=user_uuid)
        if len(houses) == 0:
            return Response.NO_AUTH
        else:
            cache.expire(token, timeout=TOKEN_MAX_EXIST_TIME)
            return Response.list_house_response(houses=houses)


def update_house(request):
    if not request.is_ajax():
        house_uuid = request.GET.get('house_uuid')
        name = request.GET.get('name')
        description = request.GET.get('description')
        token = request.GET.get('token')
    else:
        house_uuid = request.POST.get('house_uuid')
        name = request.POST.get('name')
        description = request.POST.get('description')
        token = request.POST.get('token')

    user_uuid = cache.get(token)
    if user_uuid is None:
        return Response.TOKEN_ERROR
    else:
        house = House.objects.get(uuid=house_uuid)
        if len(house) == 0:
            return Response.NO_AUTH
        else:
            house.update_one(set_name=name, set_description=description)
            cache.expire(token, timeout=TOKEN_MAX_EXIST_TIME)
            return Response.OPERATION_SUCCESSFULLY


def get_name_by_uuid(request):
    house_uuid = request.GET.get('house_uuid')
    house = House.objects.get(uuid=house_uuid)
    return Response.HouseName_Response(house.name)


def index(request):
    if request.method == 'POST':
        return add_house(request)
    # elif request.method == 'DELETE':
    #     return delete_house(request)
    elif request.method == 'GET':
        return list_houses(request)
    elif request.method == 'PATCH':
        return update_house(request)
    else:
        return Response.METHOD_ERROR
