from engineering.models import User
from engineering import Response
from engineering.utils import *
from engineering.Serializers import *
from engineering.config import *
from django.core.cache import cache


# Create your views here.
def login(request):
    if not request.method == 'POST':
        return Response.METHOD_ERROR
    else:
        if request.is_ajax():
            email = request.POST.get('email')
            password = request.POST.get('password')
        else:
            email = request.GET.get('email')
            password = request.GET.get('password')

        # requestJSONData = json.loads(request.body.decode())
        # email = requestJSONData['email']
        # password = requestJSONData['password']

        # data = JSONParser.parse(request)

        # 参数是否存在
        if email == '' or password == '' or email is None or password is None:
            return Response.PARAMS_BIND_ERROR

        # 验证邮箱格式
        if not verify_email_format(email):
            return Response.EMAIL_FORMAT_ERROR

        # 验证用户密码
        try:
            login_user = User.objects.get(Q(email=email) & Q(password=password))
        except DoesNotExist:
            return Response.USER_NOT_EXIST

        token = get_token()
        user_uuid = login_user.uuid
        cache.set(token, user_uuid, timeout=TOKEN_MAX_EXIST_TIME)
        return Response.token_response(user_uuid, login_user.nick_name, token)


def register(request):
    if not request.method == 'POST':
        return Response.METHOD_ERROR

    else:
        if request.is_ajax():
            nick_name = request.POST.get('nick_name')
            email = request.POST.get('email')
            password = request.POST.get('password')
        else:
            nick_name = request.GET.get('nick_name')
            email = request.GET.get('email')
            password = request.GET.get('password')

        # 参数是否存在
        if nick_name == '' or email == '' or password == '' or nick_name is None or email is None or password is None:
            return Response.PARAMS_BIND_ERROR

        # 验证邮箱格式
        if not verify_email_format(email=email):
            return Response.EMAIL_FORMAT_ERROR

        # 是否用户已经存在
        if User.objects.filter(Q(nick_name=nick_name) | Q(email=email)):
            return Response.USER_EXIST
        # 注册
        else:
            user = User(nick_name=nick_name, email=email, password=password)
            user.save()

            return Response.OPERATION_SUCCESSFULLY


def logout(request):
    token = request.GET.get('token')

    # 参数校验
    if token == '' or token is None:
        return Response.PARAMS_BIND_ERROR
    else:
        cache.delete_pattern(token)
        return Response.LOGOUT
