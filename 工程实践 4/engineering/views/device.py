from django.core.cache import cache
from engineering.models import *
from engineering import Response
from engineering.utils import *
from engineering.config import *


def add_device(request):
    if not request.is_ajax():
        name = request.GET.get('name')
        description = request.GET.get('description')
        oem = request.GET.get('oem')
        token = request.GET.get('token')
        house_uuid = request.GET.get('house_uuid')
        try:
            price = float(request.GET.get('price'))
        except ValueError:
            return Response.PARAMS_BIND_ERROR
    else:
        name = request.POST.get('name')
        description = request.POST.get('description')
        oem = request.POST.get('oem')
        token = request.POST.get('token')
        house_uuid = request.POST.get('house_uuid')
        try:
            price = float(request.POST.get('price'))
        except ValueError:
            return Response.PARAMS_BIND_ERROR

    if name == '' or description == '' or oem == '' or token == '' or house_uuid == '' or name is None or description is None or token is None or house_uuid is None:
        return Response.PARAMS_BIND_ERROR

    DEVICE_CATEG = ['Gas', 'Fire', 'Water']
    if name not in DEVICE_CATEG:
        return Response.DEVICE_CATEG_ERROR

    user_uuid = cache.get(token)
    if user_uuid is None:
        return Response.TOKEN_ERROR
    else:
        if not House.objects.filter(Q(uuid=house_uuid) & Q(user_uuid=user_uuid)):
            return Response.HOUSE_NOT_EXIST
        else:
            device = Device(name=name, oem=oem, house_uuid=house_uuid, price=price, description=description)
            device.save()
            cache.expire(token, timeout=TOKEN_MAX_EXIST_TIME)
            return Response.OPERATION_SUCCESSFULLY


def list_devices(request):
    token = request.GET.get('token')
    house_uuid = request.GET.get('house_uuid')

    if token is None or house_uuid is None or token == "" or house_uuid == "":
        return Response.PARAMS_BIND_ERROR

    user_uuid = cache.get(token)
    if user_uuid is None:
        return Response.TOKEN_ERROR
    else:
        if not House.objects.filter(Q(uuid=house_uuid) & Q(user_uuid=user_uuid)):
            return Response.NO_AUTH

        devices = Device.objects(house_uuid=house_uuid)
        cache.expire(token, timeout=TOKEN_MAX_EXIST_TIME)
        return Response.list_devices_of_house(devices)


def index(request):
    if request.method == 'POST':
        return add_device(request)
    elif request.method == 'GET':
        return list_devices(request)
    else:
        return Response.METHOD_ERROR
