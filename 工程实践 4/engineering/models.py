from mongoengine import *
import uuid
import datetime


# Create your models here.
class User(Document):
    uuid = StringField(default=str(uuid.uuid4()))
    nick_name = StringField(required=True, unique=True)
    email = StringField(required=True, unique=True)
    password = StringField(required=True)
    time = DateTimeField(default=datetime.datetime.now())
    description = StringField(default="Nothing showing for you.")


class House(Document):
    uuid = StringField(default=str(uuid.uuid4()))
    name = StringField(required=True)
    user_uuid = StringField(required=True)
    description = StringField(required=False, default='Nothing showing for you.')
    time = DateTimeField(default=datetime.datetime.now())


class Device(Document):
    uuid = StringField(default=str(uuid.uuid4()))
    house_uuid = StringField(required=True)
    name = StringField(required=True)
    description = StringField(required=True)
    price = FloatField(required=True)
    oem = StringField(required=True)
    time = DateTimeField(default=datetime.datetime.now())


class Record(Document):
    uuid = StringField(default=str(uuid.uuid4()))
    device_uuid = StringField(required=True)
    value = FloatField(required=True)
    time = DateTimeField(default=datetime.datetime.now())
