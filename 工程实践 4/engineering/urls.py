from django.urls import path
from .views import user, house, device, records

urlpatterns = [
    # Page

    # User
    path('user/login', user.login, name='user_login'),
    path('user/register', user.register, name='user_register'),
    path('user/logout', user.logout, name='user_logout'),

    # House
    path('house', house.index, name='house_index'),
    path('house/name', house.get_name_by_uuid, name="house_name_uuid"),

    # Device
    path('house/device', device.index, name='device_index'),

    # Record
    path('house/device/records', records.index, name='record_index'),
    path('house/device/latest_record', records.get_latest)
]
