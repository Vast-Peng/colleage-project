import re
import os
import hashlib
import random


def verify_email_format(email):
    re_email1 = re.compile(r'^\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}$')
    if re_email1.match(email):
        return True
    else:
        return False


def get_token():
    return hashlib.sha1(os.urandom(24)).hexdigest()
