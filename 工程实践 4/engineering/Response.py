from django.http import HttpResponse
import json

METHOD_ERROR = HttpResponse(json.dumps({'code': -1, 'message': 'Method Error'}))
PARAMS_BIND_ERROR = HttpResponse(json.dumps({'code': -2, 'message': 'Params bind error.'}))
OPERATION_SUCCESSFULLY = HttpResponse(json.dumps({'code': 0, 'message': 'Operate successfully'}))
USER_EXIST = HttpResponse(json.dumps({'code': -3, 'message': 'User exist'}))
USER_NOT_EXIST = HttpResponse(json.dumps({'code': -5, 'message': 'User Not FOUND'}))
EMAIL_FORMAT_ERROR = HttpResponse(json.dumps({'code': -4, 'message': 'Email Format Error'}))
LOGOUT = HttpResponse(json.dumps({'code': 0, 'message': 'Logout successfully'}))
TOKEN_ERROR = HttpResponse(json.dumps({'code': -6, 'message': 'Token Error'}))
NO_AUTH = HttpResponse(json.dumps({'code': -7, 'message': 'No Auth'}))
HOUSE_NOT_EXIST = HttpResponse(json.dumps({'code': -8, 'message': 'House Not Exist'}))
DEVICE_NOT_EXIST = HttpResponse(json.dumps({'code': -9, 'message': 'Device Not Exist'}))
DEVICE_CATEG_ERROR = HttpResponse(json.dumps({'code': -10, 'message': 'Device category error'}))


def HouseName_Response(name):
    return HttpResponse(json.dumps({
        'code': 0,
        'message': 'operate successfully',
        'content': {
            'house_name': name
        }
    }
    ))


def token_response(uuid, nick_name, token):
    res = json.dumps({
        'code': 0,
        'message': 'Login Successfully',
        'content': {
            'user_uuid': uuid,
            'nick_name': nick_name,
            'token': token
        }
    })

    return HttpResponse(res)


def list_records_of_device(records):
    _records = []
    for record in records:
        record_time = str(record.time).split('.')
        _records.append({
            'value': record.value,
            'record_time': record_time[0]
        })

    return HttpResponse(json.dumps({
        'code': 0,
        'message': 'all records',
        'record': _records
    }))


def latest_record(value):
    return HttpResponse(json.dumps({
        'code': 0,
        'message': 'ok',
        'latest': {
            'value': value
        }
    }))


def list_devices_of_house(devices):
    _devices = []
    for device in devices:
        _devices.append({
            'uuid': device.uuid,
            'name': device.name,
            'price': device.price,
            'oem': device.oem,
            'time': str(device.time),
            'description': device.description
        })

    return HttpResponse(json.dumps({
        'code': 0,
        'message': 'all devices of house',
        'devices': _devices
    }))


def list_house_response(houses):
    _houses = []
    for house in houses:
        _houses.append({
            'uuid': house.uuid,
            'name': house.name,
            'description': house.description
        })

    return HttpResponse(json.dumps({
        'code': 0,
        'message': 'all house',
        'houses': _houses
    }))
