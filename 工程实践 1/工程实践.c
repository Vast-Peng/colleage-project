﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<windows.h>
#pragma comment(lib,"winmm.lib")

#define LEN sizeof(struct Songs)
#define _LEN sizeof(struct Albums)
#define LEN_ sizeof(struct Plays)

char catg[5], ch[2];

struct Songs
{
	int number;
	char name_Song[10];
	char name_Singer[10];
	char name_Album[10];
	char name_Category[10];
}songs[50];

struct Albums
{
	char name_Album[10];
	char datatime[10];
	char name_Singer[10];
}albums[50];

struct Plays
{
	int currentNumber;
	int number;
	char name[10];
}plays[50];

void Delay(void);
void menu_Imfo(void);
void menu_Function(void);
void add_Songs(void);
void add_Albums(void);
void add_Plays(void);
void search_Songs(void);
void search_Albums(void);
void search_Plays(void);
void delete_Songs(void);
void delete_Albums(void);
void delete_Plays(void);
void modify_Songs(void);
void modify_Albums(void);
void modify_Plays(void);

int main()
{
	int n;
	
	 mciSendString("play 1.mp3",0,0,0);
	system("mode con cols=75");
	system("mode con lines=25");
	while(1)
	{
Start:
		menu_Imfo();
		scanf("%d", &n);
		switch(n)
		{
			case 1:
				strcpy(catg, "歌曲");break;
			case 2:
				strcpy(catg, "专辑");;break;
			case 3:
				strcpy(catg, "点播");;break;
			case 4:return 0;break;
			default:
				printf("Input Error!\n");
				Delay();
				continue;break;
		}
		while(1)
		{
			menu_Function();
			scanf("%d", &n);
			switch(n)
			{
				case 1:
					if(strcmp(catg, "歌曲") == 0)
						add_Songs();
					else if(strcmp(catg, "专辑") == 0)
						add_Albums();
					else if(strcmp(catg, "点播") == 0)
						add_Plays();break;
				case 2:
					if(strcmp(catg, "歌曲") == 0)
						search_Songs();
					else if(strcmp(catg, "专辑") == 0)
						search_Albums();
					else if(strcmp(catg, "点播") == 0)
						search_Plays();
						Delay();break;
				case 3:
					if(strcmp(catg, "歌曲") == 0)
						modify_Songs();
					else if(strcmp(catg, "专辑") == 0)
						modify_Albums();
					else if(strcmp(catg, "点播") == 0)
						modify_Plays();
						Delay();break;
				case 4:
					if(strcmp(catg, "歌曲") == 0)
						delete_Songs();
					else if(strcmp(catg, "专辑") == 0)
						delete_Albums();
					else if(strcmp(catg, "点播") == 0)
						delete_Plays();;break;
				case 5:goto Start;break;
				default:
					printf("Input Error!\n");
					Delay();break;
			}
		}
	}
	return 0;
}

void Delay(void)
{
	getchar();
	getchar();
}

void menu_Imfo(void)
{
	system("cls");
	printf("\n\n\n\n\n");
	printf("\t\t|---------音乐管理系统----------|\t\t\n");
	printf("\t\t|\t1.歌      曲\t\t|\n");
	printf("\t\t|\t2.专      辑\t\t|\n");
	printf("\t\t|\t3.点      播\t\t|\n");
	printf("\t\t|\t4.退      出\t\t|\n");
	printf("\t\t|-------------------------------|\t\t\n");
	printf("\n");
	printf("\t\tplease input the number:");	
}

void menu_Function(void)
{
	system("cls");
	printf("\n\n\n\n\n");
	printf("\t\t|---------音乐管理系统----------|\t\t\n");
	printf("\t\t|\t1.增      加\t\t|\n");
	printf("\t\t|\t2.查      询\t\t|\n");
	printf("\t\t|\t3.修      改\t\t|\n");
	printf("\t\t|\t4.删      除\t\t|\n");
	printf("\t\t|\t5.返      回\t\t|\n");
	printf("\t\t|-------------------------------|\t\t\n");
	printf("\n");
	printf("\t\tplease input the number:");		
}

void add_Songs(void)
{
	FILE *fp;
	int i, m = 0;
	if((fp = fopen("songs.dat", "ab+")) == NULL)
	{
		printf("The songs.dat can`t open!\n");
		Delay();
		fclose(fp);
		return;
	}
	while(!feof(fp))
		if(fread(&songs[m], LEN, 1, fp) == 1)
			m++;
	system("cls");
	if( m == 0)
		printf("No data!\n");
	printf("Do you want to add?<y/n>");
	scanf("%s", ch);
	while(strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
	{
		printf("编号:");
		scanf("%d", &songs[m].number);
		for(i = 0; i < m; i++)
			if(songs[m].number == songs[i].number)
			{
				printf("The number has alreadt existed!\n");
				Delay();
				fclose(fp);
				return;
			}
		printf("歌曲名称:");
		scanf("%s", songs[m].name_Song);
		printf("歌手名称:");
		scanf("%s", songs[m].name_Singer);
		printf("专辑名称:");
		scanf("%s", songs[m].name_Album);
		printf("歌曲类别:");
		scanf("%s", songs[m].name_Category);
		if(fwrite(&songs[m], LEN, 1, fp) != 1)
		{
			printf("The data can`t be written!\n");
			Delay();
			fclose(fp);
			return;
		}
		printf("No. %d succeed!    \n", songs[m].number);
		m++;
		printf("Continue?<y/n>:");
		scanf("%s", ch);
	}
	fclose(fp);
	return;
}

void add_Albums(void)
{
	FILE *fp;
	int i, m = 0;
	if((fp = fopen("albums.dat", "ab+")) == NULL)
	{
		printf("The albums.dat can`t open!\n");
		Delay();
		return;
	}
	while(!feof(fp))
		if(fread(&albums[m], LEN, 1, fp) == 1)
			m++;
	system("cls");
	if( m == 0)
		printf("No data!\n");
	printf("Do you want to add?<y/n>:");
	scanf("%s", ch);
	while(strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
	{
		printf("专辑名称:");
		scanf("%s", albums[m].name_Album);
		for(i = 0; i < m; i++)
			if(strcmp(albums[m].name_Album, albums[i].name_Album) == 0)
			{
				printf("The name has alreadt existed!\n");
				Delay();
				fclose(fp);
				return;
			}
		printf("发布时间:");
		scanf("%s", albums[m].datatime);
		printf("歌手名称:");
		scanf("%s", albums[m].name_Singer);
		if(fwrite(&albums[m], LEN, 1, fp) != 1)
		{
			printf("The data can`t be written!\n");
			Delay();
			fclose(fp);
			return;
		}
		printf("the %s succeed!    \n", albums[m].name_Album);
		m++;
		printf("Continue?<y/n>:");
		scanf("%s", ch);
	}
	fclose(fp);
	return;
}
void add_Plays(void)
{
	FILE *fp1, *fp2;
	int i, m = 0, n = 0;
	if((fp1 = fopen("songs.dat", "ab+")) == NULL)
	{
		printf("Can`t open the songs.dat\n");
		Delay();
		fclose(fp1);
	}
	if((fp2 = fopen("plays.dat", "ab+")) == NULL)
	{
		printf("Can`t open the plays.dat\n");
		Delay();
		fclose(fp2);
	}
	while(!feof(fp1))
		if(fread(&songs[m], LEN, 1, fp1) == 1)
			m++;
	while(!feof(fp2))
		if(fread(&plays[n], LEN_, 1, fp2) == 1)
			n++;
	if(m == 0)
	{
		printf("No data!\n");
		Delay();
		fclose(fp1);
		fclose(fp2);
		return;
	}
	printf("please input the number you want to add:");
	scanf("%d", &plays[n].number);
	for(i = 0; i < m; i++)
		if(plays[n].number == songs[i].number)
		{
			strcpy(plays[n].name, songs[i].name_Song);
			plays[n].currentNumber = 0;
			fwrite(&plays[n], LEN_, 1, fp2);
		}
	printf("%s succeed!\n", plays[n].name);
	fclose(fp1);
	fclose(fp2);
	Delay();
}

void search_Songs(void)
{
	FILE *fp;
	int i, j, n, temp, flag = 0, m = 0;
	int l, k = 0, cookis[50];
	char keywords[10];
	
	if((fp = fopen("songs.dat", "ab+")) == NULL)
	{
		printf("The songs.dat can`t open!\n");
		Delay();
		fclose(fp);
		return;
	}
	while(!feof(fp))
		if(fread(&songs[m], LEN, 1, fp) == 1)
			m++;
	system("cls");
	if(m == 0)
	{
		printf("No data!\n");
		Delay();
		fclose(fp);
		return;
	}
	printf("请选择查询方式:\n1.编号\t2.字段\nplease input:");
	scanf("%d", &n);
	if(n == 1)
	{
		printf("编号:");
		scanf("%d", &temp);
		for(i = 0; i < m; i++)
			if(songs[i].number == temp)
			{
				flag = 1;
				break;
			}
		if(flag)
		{
			printf("编号\t歌曲名称\t歌手名称\t专辑名称\t歌曲类别\n\n");
			printf("%2d%12s%16s%17s%17s", songs[i].number, songs[i].name_Song, songs[i].name_Singer, songs[i].name_Album, songs[i].name_Category);
			fclose(fp);
			return;
		}
		else
		{
			printf("No data!\n");
			Delay();
			fclose(fp);
			return;
		}
	}
	else if(n == 2)
	{
		printf("字段:");
		scanf("%s", keywords);
		for(i = 0; i < m; i++)
		{
			if(strcmp(keywords, songs[i].name_Song) == 0)
				flag = 1;
			else if(strcmp(keywords, songs[i].name_Album) == 0)
				flag = 1;
			else if(strcmp(keywords, songs[i].name_Singer) == 0)
				flag = 1;
			else if(strcmp(keywords, songs[i].name_Category) == 0)
				flag = 1;
			if(flag)
			{
				flag = 0;
				cookis[k] = i;
				k++;
			}
		}
		if(k == 0)
		{
			printf("No data!\n");
			Delay();
			return;
		}
		else
		{
			for(i = 0; i < k; i++)
				for(j = 0; j < k - 1 - i; j++)
					if(songs[cookis[j]].number > songs[cookis[j + 1]].number)
					{
						l = cookis[j];
						cookis[j] = cookis[j + 1];
						cookis[j + 1] = l;
					}
			printf("编号\t歌曲名称\t歌手名称\t专辑名称\t歌曲类别\n\n");
			for(i = 0; i < k; i++)
			{
				printf("%2d%12s%16s%17s%17s\n", songs[cookis[i]].number, songs[cookis[i]].name_Song, songs[cookis[i]].name_Singer, songs[cookis[i]].name_Album, songs[cookis[i]].name_Category);	
			}
		}
	}
	else
	{
		printf("Input Error!\n");
		Delay();
		fclose(fp);
		return;
	}
	fclose(fp);
	Delay();
}

void search_Albums(void)
{
	FILE *fp;
	int i, j = 0, temp[50], flag = 0,m = 0;
	char keywords[10];
	if((fp = fopen("albums.dat", "ab+")) == NULL)
	{
		printf("Can`t open albums.dat\n");
		Delay();
		fclose(fp);
		return;
	}
	while(!feof(fp))
		if(fread(&albums[m], _LEN, 1, fp) == 1)
			m++;
	system("cls");
	if(m == 0)
	{
		printf("No data!\n");
		fclose(fp);
		return;
	}
	printf("关键字:");
	scanf("%s", keywords);
	for(i = 0; i < m; i++)
	{
		if(strcmp(keywords, albums[i].name_Album) == 0)
			flag = 1;
		else if(strcmp(keywords, albums[i].datatime) == 0)
			flag = 1;
		else if(strcmp(keywords, albums[i].name_Singer) == 0)
			flag = 1;
		if(flag == 1)
		{
			flag = 0;
			temp[j] = i;
			j++;
		}
	}
	if(j == 0)
	{
		printf("No data!\n");
		Delay();
		return;
	}
	else
	{
		printf("专辑名称\t发布时间\t歌手名称\n\n");
		for(i = 0; i < j; i++)
			printf("%6s%20s%16s\n", albums[temp[i]].name_Album, albums[temp[i]].datatime, albums[temp[i]].name_Singer);
	}
	fclose(fp);
}

void search_Plays(void)
{
	FILE *fp;
	int i, m = 0;
	if((fp = fopen("plays.dat", "ab+")) ==NULL)
	{
		printf("Can`t open the plays.dat\n");
		Delay();
		fclose(fp);
		return;
	}
	while(!feof(fp))
		if(fread(&plays[m], LEN_, 1, fp) == 1)
			m++;
	system("cls");
	printf("当前编号\t歌曲编号\t歌曲名称\n\n");
	for(i = 0; i < m; i++)
		printf("%2d%18d%18s\n",i + 1, plays[i].number, plays[i].name);
	fclose(fp);
}

void delete_Songs(void)
{
	FILE *fp;
	int i, n, m = 0;
	if((fp = fopen("songs.dat", "ab+")) == NULL)
	{
		printf("Can`t open the songs.dat\n");
		Delay();
		return;
	}
	while(!feof(fp))
		if(fread(&songs[m], LEN, 1, fp) == 1)
			m++;
	fclose(fp);
	if(m == 0)
	{
		printf("No data!\n");
		Delay();
		return;
	}
	system("cls");
	remove("songs.dat");
	fp = fopen("songs.dat", "ab+");
	printf("input the number you want to delete:");
	scanf("%d", &n);
	for(i = 0; i < m; i++)
		if(songs[i].number != n)
			fwrite(&songs[i], LEN, 1, fp);
		else
			continue;
	printf("OK！\n");
	Delay();
	fclose(fp);
}

void delete_Albums(void)
{
	FILE *fp;
	int i, m = 0;
	char name[10];
	if((fp = fopen("albums.dat", "ab+")) == NULL)
	{
		printf("Can`t open the albums.dat\n");
		Delay();
		return;
	}
	while(!feof(fp))
		if(fread(&albums[m], _LEN, 1, fp) == 1)
			m++;
	fclose(fp);
	if(m == 0)
	{
		printf("No data!\n");
		Delay();
		return;
	}
	system("cls");
	remove("albums.dat");
	fp = fopen("albums.dat", "ab+");
	printf("input the name you want to delete:");
	scanf("%s", name);
	for(i = 0; i < m; i++)
		if(strcmp(name, albums[i].name_Album) != 0)
			fwrite(&albums[i], _LEN, 1, fp);
		else
			continue;
	printf("OK！\n");
	Delay();
	fclose(fp);
}

void delete_Plays(void)
{
	FILE *fp;
	int i, temp, m = 0;
	if((fp = fopen("plays.dat", "ab+")) == NULL)
	{
		printf("Can`t open the plays.dat");
		fclose(fp);
		Delay();
		return;
	}
	while(!feof(fp))
		if(fread(&plays[m], LEN_, 1, fp) == 1)
			m++;
	fclose(fp);
	if(m == 0)
	{
		printf("No data!\n");
		Delay();
		return;
	}
	remove("plays.dat");
	fp = fopen("plays.dat", "ab+");
	printf("input the number you want to delete:");
	scanf("%d", &temp);
	for(i = 0; i < m; i++)
		if(temp == plays[i].number)
			continue;
		else
			fwrite(&plays[i], LEN_, 1, fp);
	printf("OK!\n");
	Delay();
	fclose(fp);
}

void modify_Songs(void)
{
	FILE *fp;
	int i, n, m = 0;
	
	if((fp = fopen("songs.dat", "ab+")) == NULL)
	{
		printf("Can`t open the songs.dat\n");
		Delay();
		fclose(fp);
		return;
	}
	while(!feof(fp))
		if(fread(&songs[m], LEN, 1, fp) == 1)
			m++;
	fclose(fp);
	if(m == 0)
	{
		printf("No data!\n");
		Delay();
		return;
	}
	remove("songs.dat");
	fp =fopen("songs.dat", "ab+");
	system("cls");
	printf("please input the number you want to modify:");
	scanf("%d", &n);
	for(i = 0; i < m; i++)
		if(songs[i].number != n)
			fwrite(&songs[i], LEN, 1, fp);
		else if(songs[i].number == n)
		{
			printf("歌曲名称:");
			scanf("%s", songs[i].name_Song);
			printf("歌手名称:");
			scanf("%s", songs[i].name_Singer);
			printf("专辑名称:");
			scanf("%s", songs[i].name_Album);
			printf("歌曲类别:");
			scanf("%s", songs[i].name_Category);
			if(fwrite(&songs[i], LEN, 1, fp) == 1)
				printf("OK!\n");;
		}
	fclose(fp);
	Delay();
}

void modify_Albums(void)
{
	FILE *fp;
	int i, m = 0;
	char name[10];
	
	if((fp = fopen("albums.dat", "ab+")) == NULL)
	{
		printf("Can`t open the albums.dat\n");
		Delay();
		fclose(fp);
		return;
	}
	while(!feof(fp))
		if(fread(&albums[m], _LEN, 1, fp) == 1)
			m++;
	fclose(fp);
	if(m == 0)
	{
		printf("No data!\n");
		Delay();
		return;
	}
	remove("albums.dat");
	fp =fopen("albums.dat", "ab+");
	system("cls");
	printf("please input the name you want to modify:");
	scanf("%s", name);
	for(i = 0; i < m; i++)
		if(strcmp(name, albums[i].name_Album) != 0)
			fwrite(&albums[i], _LEN, 1, fp);
		else if(strcmp(name, albums[i].name_Album) == 0)
		{
			printf("专辑名称:");
			scanf("%s", albums[i].name_Album);
			printf("发布时间:");
			scanf("%s", albums[i].datatime);
			printf("歌手名称:");
			scanf("%s", albums[i].name_Singer);
			if(fwrite(&albums[i], _LEN, 1, fp) == 1)
				printf("OK!\n");
		}
	fclose(fp);
	Delay();
}

void modify_Plays(void)
{
	FILE *fp;
	int i, _temp, temp, m = 0;
	if((fp = fopen("plays.dat", "ab+")) ==NULL)
	{
		printf("Can`t open the plays.dat\n");
		Delay();
		fclose(fp);
		return;
	}
	while(!feof(fp))
		if(fread(&plays[m], LEN_, 1, fp) == 1)
			m++;
	printf("input the number you want to modify:");
	scanf("%d", &temp);
	if(temp > m || temp <= 0)
	{
		printf("Input Error!\n");
		fclose(fp);
		Delay();
		return;
	}
	fclose(fp);
	remove("plays.dat");
	fp = fopen("plays.dat", "ab+");
	printf("input the number you want to move to:");
	scanf("%d", &_temp);
	for(i = 1; i <= m; i++)
		if(i == _temp)
			fwrite(&plays[temp - 1], LEN_, 1, fp);
		else if(i == temp)
			fwrite(&plays[_temp - 1], LEN_, 1, fp);
		else
			fwrite(&plays[i - 1], LEN_, 1, fp);
	printf("OK!\n");
	fclose(fp);
}