# 导入第三方库
import serial  
import binascii
import serial.tools.list_ports
import time
import re
from pymongo import MongoClient

client = MongoClient('localhost', 27017) # 连接 MongoDB 数据库
home = client['home'] # 数据库名称
zigbee = home['zigbee'] # 文档名称

class MSerial(object):

    def __init__(self, buand=9600, timeout=60):
        self.__buand = buand # 波特率 默认 9600
        self.__timeout = timeout # 超时
  
        self.__plist = list(serial.tools.list_ports.comports()) # 列举所有可用的串口
        if len(self.__plist) < 0: # 如果没有
            print('No data')
            exit() # 推出
        self.__plist_0 = list(self.__plist[0]) # 因为只用到一个串口，就直接取第 0 个
        self.__serialName = self.__plist_0[0]  # 串口名字
        self.__serialFd = serial.Serial(self.__serialName, self.__buand, timeout=self.__timeout)  # 打开串口

    def read_data(self):
        data = b'' # 创建字节串变量
        for i in range(32): # zigbee 一次发送 32 字节
            data += self.__serialFd.read() # 读取
        return data # 返回数据

def ByteToHex( bins ):
    """
    Convert a byte string to it's hex string representation e.g. for output.
    """

    return ''.join( [ "%02X:" % x for x in bins ] ).strip().strip(':') # 字节 转 十六进制字符串

# 一次只读取 10 次数据
for i in range(11):
    data = MSerial().read_data() # 创建对象，并调用 read_data()
    net = ByteToHex(data[4:14]) # 网络地址
    temp = str(data[14:16], encoding='utf-8') # 传感器类型
    if temp == 'xx':
        continue
    dic = {
        'iee_adress': net[0:23], # 64 位 ieee 地址
        'net_adress': net[24:], # 16 位网络地址
        'type': temp, # 类型
        'data': str(data[16:19], encoding='utf-8'), # 数据
        'date': time.strftime('%Y-%m-%d %H:%M:%S') # 时间
        }
    print(dic) # 打印数据
    zigbee.insert_one(dic) # 保存到数据库
