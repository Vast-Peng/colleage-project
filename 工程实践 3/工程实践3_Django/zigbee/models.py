from mongoengine import *
from mongoengine import connect


# Create your models here.
class zigbee(Document):
    iee_adress = StringField()
    net_adress = StringField()
    type = StringField()
    data = StringField()
    date = StringField()
