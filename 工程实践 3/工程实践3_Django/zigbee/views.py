from django.shortcuts import render
from django.http import HttpResponse
from zigbee.models import zigbee
from mongoengine import *

connect('home')


# Create your views here.
def index(request):

    zigbee_type = request.GET.get('zigbee')
        
    if zigbee_type == 'gas':
        gas = zigbee.objects.filter(type='QT')
        return render(request, 'zigbee/more_info.html', context={'gas': gas, 'title': zigbee_type})
    elif zigbee_type == 'temp':
        temperature = zigbee.objects.filter(type='WD')
        return render(request, 'zigbee/more_info.html', context={'gas': temperature, 'title': zigbee_type})
    elif zigbee_type == 'humi':
        humi = zigbee.objects.filter(type='SD')
        return render(request, 'zigbee/more_info.html', context={'gas': humi, 'title': zigbee_type})
    else:
        return render(request, 'zigbee/more_info.html')
    

