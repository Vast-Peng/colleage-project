/**************************************************************************************************
  Filename:       SampleApp.c
  Revised:        $Date: 2009-03-18 15:56:27 -0700 (Wed, 18 Mar 2009) $
  Revision:       $Revision: 19453 $

  Description:    Sample Application (no Profile).


  Copyright 2007 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED 揂S IS?WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
  This application isn't intended to do anything useful, it is
  intended to be a simple example of an application's structure.

  This application sends it's messages either as broadcast or
  broadcast filtered group messages.  The other (more normal)
  message addressing is unicast.  Most of the other sample
  applications are written to support the unicast message model.

  Key control:
    SW1:  Sends a flash command to all devices in Group 1.
    SW2:  Adds/Removes (toggles) this device in and out
          of Group 1.  This will enable and disable the
          reception of the flash command.
*********************************************************************/



/*********************************************************************
 * INCLUDES
 */
#include "OSAL.h"
#include "ZGlobals.h"
#include "AF.h"
#include "aps_groups.h"
#include "ZDApp.h"

#include "SampleApp.h"
#include "SampleAppHw.h"

#include "OnBoard.h"

/* HAL */
#include "hal_lcd.h"
#include "hal_led.h"
#include "hal_key.h"
#include "string.h"
#include    <stdio.h>
#include "MT_UART.h"
#include "HAL_ADC.h"

#include "sensor.h"
#include "SHT10.h"
#include "gas.h"
#include "hal_timer34.h"
#include "LcdDisp.h"
/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

// This list should be filled with Application specific Cluster IDs.
const cId_t SampleApp_ClusterList[SAMPLEAPP_MAX_CLUSTERS] =
{
  SAMPLEAPP_PERIODIC_CLUSTERID,
  SAMPLEAPP_FLASH_CLUSTERID
};

const SimpleDescriptionFormat_t SampleApp_SimpleDesc =
{
  SAMPLEAPP_ENDPOINT,              //  int Endpoint;
  SAMPLEAPP_PROFID,                //  uint16 AppProfId[2];
  SAMPLEAPP_DEVICEID,              //  uint16 AppDeviceId[2];
  SAMPLEAPP_DEVICE_VERSION,        //  int   AppDevVer:4;
  SAMPLEAPP_FLAGS,                 //  int   AppFlags:4;
  SAMPLEAPP_MAX_CLUSTERS,          //  uint8  AppNumInClusters;
  (cId_t *)SampleApp_ClusterList,  //  uint8 *pAppInClusterList;
  SAMPLEAPP_MAX_CLUSTERS,          //  uint8  AppNumInClusters;
  (cId_t *)SampleApp_ClusterList   //  uint8 *pAppInClusterList;
};

// This is the Endpoint/Interface description.  It is defined here, but
// filled-in in SampleApp_Init().  Another way to go would be to fill
// in the structure here and make it a "const" (in code space).  The
// way it's defined in this sample app it is define in RAM.
endPointDesc_t SampleApp_epDesc;

/*********************************************************************
 * EXTERNAL VARIABLES
 */
extern unsigned char SensorNum;
unsigned char Relay1State = 0;
unsigned char Relay2State = 0;


/*********************************************************************
 * EXTERNAL FUNCTIONS
 */


/*********************************************************************
 * LOCAL VARIABLES
 */
uint8 SampleApp_TaskID;   // Task ID for internal task/event processing
                          // This variable will be received when
                          // SampleApp_Init() is called.
devStates_t SampleApp_NwkState;


uint8 SampleApp_TransID;  // This is the unique message ID (counter)
uint8 *ieeeAddr;//物理地址

uint16 Auto_Temp_Time = 5;//温度传感器自动上传间隔
uint16 Auto_Temp_Count = 0;//温度传感器自动上传计数器
uint8 Auto_Temp_Flag = 0;//温度传感器自动上传标致位



union f1{
  uint8 RxBuf[32];
  struct UARTCOMBUF
  {
        uint8 Head;       //头
        uint8 HeadCom[3]; //命令头
        uint8 Laddr[8];   //物理地址
        uint8 Saddr[2];   //网络地址
        uint8 DataBuf[16];  //数据缓冲区
        uint8 CRC;    //校验位
  	uint8 LastByte;//帧尾
  }RXDATA;
}UartRxBuf;//从串口接收到的数据帧


union e{
  uint8 TxBuf[32];
  struct UARTBUF
  {
        uint8 Head;       //头
        uint8 HeadCom[3]; //命令头
        uint8 Laddr[8];   //物理地址
        uint8 Saddr[2];   //网络地址
        uint8 DataBuf[16];  //数据缓冲区
        uint8 CRC;    //校验位
  	uint8 LastByte;//帧尾
  }TXDATA;
}UartTxBuf;//从串口发送数据帧


union h{
  uint8 RxBuf[32];
  struct RFRXBUF
  {
  	uint8 Head;       //头
        uint8 HeadCom[3]; //命令头
        uint8 Laddr[8];
        uint8 Saddr[2];
        uint8 DataBuf[16];  //数据缓冲区
	uint8 CRC;    //校验位
  	uint8 LastByte;//帧尾
  }RXDATA;
}RfRx;//无线接收缓冲区


union j{
  uint8 TxBuf[32];
  struct RFTXBUF
  {
  	uint8 Head;       //头
	uint8 HeadCom[3]; //命令头
	uint8 Laddr[8];   //物理地址
	uint8 Saddr[2];   //网络地址
	uint8 DataBuf[16];  //数据缓冲区
	uint8 CRC;    //校验位
	uint8 LastByte;//帧尾
  }TXDATA;
}RfTx;//无线发送缓冲区


uint16 Ultrasonic_Count;//超声波计数

/*********************************************************************
 * LOCAL FUNCTIONS
 */
void SampleApp_HandleKeys( uint8 shift, uint8 keys );
void SampleApp_MessageMSGCB( afIncomingMSGPacket_t *pckt );
uint8 SendData( uint16 addr, uint8 *buf, uint8 Leng);
void UartRxComCallBack(void);
uint8 CheckUartData(uint8 *arr, uint8 n);
void WaitUs(uint16 microSecs);
void UartTX_Send_String(uint8 *Data,int len);
/*********************************************************************
 * NETWORK LAYER CALLBACKS
 */

/*********************************************************************
 * PUBLIC FUNCTIONS
 */


/*****************************************************************************
  void WaitUs(uint16 microSecs)

  延时uS函数.
*****************************************************************************/
void WaitUs(uint16 microSecs)
{
  while(microSecs--)
  {
    /* 32 NOPs == 1 usecs */
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop");
  }
}


/****************************************************************
*函数功能 ：串口发送字符串函数					
*入口参数 : data:数据									
*			len :数据长度							
*返 回 值 ：无											
*说    明 ：				
****************************************************************/
void UartTX_Send_String(uint8 *Data,int len)
{
  int j;
  for(j=0; j<len; j++)
  {
    U0DBUF = *Data++;
    while(UTX0IF == 0);
    UTX0IF = 0;
  }
}

/*****************************************************************************
  uint8 SendData( uint16 addr, uint8 *buf, uint8 Leng)

  发送一组数据.
*****************************************************************************/
uint8 SendData( uint16 addr, uint8 *buf, uint8 Leng)
{
	afAddrType_t SendDataAddr;
	
	SendDataAddr.addrMode = (afAddrMode_t)Addr16Bit;
	SendDataAddr.endPoint = SAMPLEAPP_ENDPOINT;
	SendDataAddr.addr.shortAddr = addr;
        if ( AF_DataRequest( &SendDataAddr, &SampleApp_epDesc,
                       2,//SAMPLEAPP_PERIODIC_CLUSTERID,
                       Leng,
                       buf,
                       &SampleApp_TransID,
                       AF_DISCV_ROUTE,
                     //  AF_ACK_REQUEST,
                       AF_DEFAULT_RADIUS ) == afStatus_SUCCESS )
	{
		return 1;
	}
	else
	{
		return 0;// Error occurred in request to send.
	}
}



/*****************************************************************************
  uint8 CheckUartData(uint8 *arr, uint8 n)

  数据校验,这里做的是加和校验，正确返回1,错误返回0
*****************************************************************************/
uint8 CheckUartData(uint8 *arr, uint8 n)
{
	uint8 sum=0;
	uint8 i;
	for(i=0; i<n; i++)
	{
		sum += *arr;
		arr++;
	}
	return sum;
}




/*********************************************************************
 * @fn      SampleApp_Init
 *
 * @brief   Initialization function for the Generic App Task.
 *          This is called during initialization and should contain
 *          any application specific initialization (ie. hardware
 *          initialization/setup, table initialization, power up
 *          notificaiton ... ).
 *
 * @param   task_id - the ID assigned by OSAL.  This ID should be
 *                    used to send messages and set timers.
 *
 * @return  none
 */
void SampleApp_Init( uint8 task_id )
{
  SampleApp_TaskID = task_id;
  SampleApp_NwkState = DEV_INIT;
  SampleApp_TransID = 0;
  
  // Device hardware initialization can be added here or in main() (Zmain.c).
  // If the hardware is application specific - add it here.
  // If the hardware is other parts of the device add it in main().

#if defined ( HOLD_AUTO_START )
  // HOLD_AUTO_START is a compile option that will surpress ZDApp
  //  from starting the device and wait for the application to
  //  start the device.
  ZDOInitDevice(0);
#endif

  // Fill out the endpoint description.
  SampleApp_epDesc.endPoint = SAMPLEAPP_ENDPOINT;
  SampleApp_epDesc.task_id = &SampleApp_TaskID;
  SampleApp_epDesc.simpleDesc
            = (SimpleDescriptionFormat_t *)&SampleApp_SimpleDesc;
  SampleApp_epDesc.latencyReq = noLatencyReqs;

  // Register the endpoint description with the AF
  afRegister( &SampleApp_epDesc );

  // Register for all key events - This app will handle all key events
  RegisterForKeys( SampleApp_TaskID );

//#ifdef	WXL_COORD

  MT_UartRegisterTaskID(SampleApp_TaskID);
	
//#endif
#ifdef	WXL_COORD
  HalLedSet( HAL_LED_1,HAL_LED_MODE_ON );
#endif  
#ifdef	WXL_RFD
  LcdPutString16_8(0,0," Searching! ",12,1);
#endif  
}

/*********************************************************************
 * @fn      SampleApp_ProcessEvent
 *
 * @brief   Generic Application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id  - The OSAL assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  none
 */
uint16 SampleApp_ProcessEvent( uint8 task_id, uint16 events )
{
	afIncomingMSGPacket_t *MSGpkt;
        uint16 temp1;
        uint8 temp;
        uint16 sht;
        float sht11;
        char msg[12];
       
        
#if (defined(WXL_ROUTER) || defined(WXL_RFD))//ROUTER OR RFD
	uint16 SrcSaddr;
#endif
	
	(void)task_id;  // Intentionally unreferenced parameter

	if ( events & SYS_EVENT_MSG )
	{
		MSGpkt = (afIncomingMSGPacket_t *)osal_msg_receive( SampleApp_TaskID );
		while ( MSGpkt )
		{
			switch ( MSGpkt->hdr.event )
			{
				// Received when a key is pressed
				case KEY_CHANGE:
					SampleApp_HandleKeys( ((keyChange_t *)MSGpkt)->state, ((keyChange_t *)MSGpkt)->keys );
					break;

				// Received when a messages is received (OTA) for this endpoint
				case AF_INCOMING_MSG_CMD:
					SampleApp_MessageMSGCB( MSGpkt );
					break;

				// Received whenever the device changes state in the network
				case ZDO_STATE_CHANGE:
					SampleApp_NwkState = (devStates_t)(MSGpkt->hdr.status);
					if ( (SampleApp_NwkState == DEV_ZB_COORD)
						|| (SampleApp_NwkState == DEV_ROUTER)
						|| (SampleApp_NwkState == DEV_END_DEVICE) )
					{
						HalLedSet( HAL_LED_1,HAL_LED_MODE_ON );		
                                                
                                                
                                                
                                                
                                                
#ifdef	WXL_RFD
						LcdPutString16_8(0,0,"Connected!!!",12,1);
                                                /*
						memset(RfTx.TxBuf,'x',32);
						RfTx.TXDATA.Head = '&';
						RfTx.TXDATA.HeadCom[0] = 'J';
						RfTx.TXDATA.HeadCom[1] = 'O';
						RfTx.TXDATA.HeadCom[2] = 'N';
						ieeeAddr = NLME_GetExtAddr();
						memcpy(RfTx.TXDATA.Laddr,ieeeAddr,8);
                                       		SrcSaddr = NLME_GetShortAddr();
						RfTx.TXDATA.Saddr[0] = SrcSaddr;
                                        	RfTx.TXDATA.Saddr[1] = SrcSaddr>>8;
						RfTx.TXDATA.DataBuf[0] = 'R';
						RfTx.TXDATA.DataBuf[1] = 'F';
						RfTx.TXDATA.DataBuf[2] = 'D';
						
						NLME_GetCoordExtAddr(&RfTx.TXDATA.DataBuf[3]);
                                                temp1 = NLME_GetCoordShortAddr();
                                                RfTx.TXDATA.DataBuf[11] = (unsigned char)(temp1>>8);
                                                RfTx.TXDATA.DataBuf[12] = (unsigned char)(temp1);
						RfTx.TXDATA.DataBuf[13] = SensorNum;
						
						RfTx.TXDATA.LastByte = '*';
					
						SendData(0x0000, RfTx.TxBuf, 32);//发送自己的节点信息到主机*/
                                                
                                                
						osal_start_timerEx( SampleApp_TaskID,
							SAMPLEAPP_SEND_PERIODIC_MSG_EVT,
							SAMPLEAPP_1000MS_TIMEOUT );//每秒检测一次传感器
#endif
					
#ifdef	WXL_ROUTER
						LcdPutString16_8(0,0,"   ROUTER   ",12,1);
						memset(RfTx.TxBuf,'x',32);
						
						RfTx.TXDATA.Head = '&';
						RfTx.TXDATA.HeadCom[0] = 'J';
						RfTx.TXDATA.HeadCom[1] = 'O';
						RfTx.TXDATA.HeadCom[2] = 'N';
						ieeeAddr = NLME_GetExtAddr();
						memcpy(RfTx.TXDATA.Laddr,ieeeAddr,8);
						SrcSaddr = NLME_GetShortAddr();
						RfTx.TXDATA.Saddr[0] = SrcSaddr;
	                                        RfTx.TXDATA.Saddr[1] = SrcSaddr>>8;
						RfTx.TXDATA.DataBuf[0] = 'R';
						RfTx.TXDATA.DataBuf[1] = 'O';
						RfTx.TXDATA.DataBuf[2] = 'U';
						
						NLME_GetCoordExtAddr(&RfTx.TXDATA.DataBuf[3]);
                                                temp1 = NLME_GetCoordShortAddr();
                                                RfTx.TXDATA.DataBuf[11] = (unsigned char)(temp1>>8);
                                                RfTx.TXDATA.DataBuf[12] = (unsigned char)(temp1);
                                                
                                                RfTx.TXDATA.DataBuf[13] = SensorNum;
                                                
                                                RfTx.TXDATA.LastByte = '*';
						
						SendData(0x0000, RfTx.TxBuf, 32);//发送自己的节点信息到主机
                                                
                                             
						osal_start_timerEx( SampleApp_TaskID,
							SAMPLEAPP_SEND_PERIODIC_MSG_EVT,
							SAMPLEAPP_SEND_PERIODIC_MSG_TIMEOUT );//每秒检测一次传感器
#endif		
					} else {
                                          LcdPutString16_8(0,0,"Disconnected",12,1);
                                        }
					break;
				
				case SPI_INCOMING_ZTOOL_PORT:
				
					;//串口收到一帖数据的处理
					
					break;
				default:
					break;
			}

			// Release the memory
			osal_msg_deallocate( (uint8 *)MSGpkt );

			// Next - if one is available
			MSGpkt = (afIncomingMSGPacket_t *)osal_msg_receive( SampleApp_TaskID );
		}

		// return unprocessed events
		return (events ^ SYS_EVENT_MSG);
	}

	// Send a message out - This event is generated by a timer
	//  (setup in SampleApp_Init()).
	if ( events & SAMPLEAPP_SEND_PERIODIC_MSG_EVT )//发送数据
	{                                 //普通温度光敏传感器
          
            
                    
                    memset(RfTx.TxBuf,'x',32);
                    RfTx.TXDATA.Head = '&';
                            
                    RfTx.TXDATA.HeadCom[0] = 'A';
                    RfTx.TXDATA.HeadCom[1] = 'A';
                    RfTx.TXDATA.HeadCom[2] = 'S';
                            
                    ieeeAddr = NLME_GetExtAddr();
                    memcpy(RfTx.TXDATA.Laddr,saveExtAddr,8);
                  
                    temp1 = NLME_GetShortAddr();     
                    RfTx.TXDATA.Saddr[0] = temp1;
                    RfTx.TXDATA.Saddr[1] = temp1>>8;
                    
                    
                    
                    // 读取温度
                    /*
                    RfTx.TXDATA.DataBuf[0] = 'W';
                    RfTx.TXDATA.DataBuf[1] = 'D';
                    temp = ReadTc77();
                    sprintf(msg, "  Temp:%03d  ", temp);
                    LcdPutString16_8(0,0,msg,12,1);
                    RfTx.TXDATA.DataBuf[2] = sht/100 + 0x30;
                    RfTx.TXDATA.DataBuf[3] = temp/10 + '0';
                    RfTx.TXDATA.DataBuf[4] = temp%10 + '0';*/
                    
                    // 读取湿度
                    /*
                    RfTx.TXDATA.DataBuf[0] = 'S';
                    RfTx.TXDATA.DataBuf[1] = 'D';
                    sht = Read_SHT1X(5);
                    sht&=0x0fff;
                    sht11 = (float)(sht*0.0405) - 4 - (sht * sht * 0.000028);
                    sht = sht11;
                    RfTx.TXDATA.DataBuf[2] = sht/100 + 0x30;
                    RfTx.TXDATA.DataBuf[3] = sht/10 + 0x30;
                    RfTx.TXDATA.DataBuf[4] = sht%10 + 0x30;
                    sprintf(msg, "  Humi:%03d  ", sht);
                    LcdPutString16_8(0,0,msg,12,1);
                    
                    // 小数位
                    /*
                    RfTx.TXDATA.DataBuf[4] = '.';
                    sht11 = sht11 * 10;
                    sht = sht11;
                    RfTx.TXDATA.DataBuf[5] = sht % 10 + 0x30;*/
                   
                    // 读取气体
                    
                    RfTx.TXDATA.DataBuf[0] = 'Q';
                    RfTx.TXDATA.DataBuf[1] = 'T';
                    temp = ReadSensorAdc(0);
                    sprintf(msg, "  Gas:%03d   ", temp);
                    LcdPutString16_8(0,0,msg,12,1);
                    RfTx.TXDATA.DataBuf[2] = temp/100 + 0x30;
                    RfTx.TXDATA.DataBuf[3] = temp/10 + 0x30;
                    RfTx.TXDATA.DataBuf[4] = temp%10 + 0x30;
                    
                    RfTx.TXDATA.LastByte = '*';
                    
                    SendData(0x0000, RfTx.TxBuf,  32);//自动上传传感器数据
            }
        
		// Setup to send message again in normal period (+ a little jitter)
		osal_start_timerEx( SampleApp_TaskID, SAMPLEAPP_SEND_PERIODIC_MSG_EVT, SAMPLEAPP_SEND_PERIODIC_MSG_TIMEOUT);
		
		return (events ^ SAMPLEAPP_SEND_PERIODIC_MSG_EVT);
	
	// Discard unknown events
	return 0;
}

/*********************************************************************
 * Event Generation Functions
 */
/*********************************************************************
 * @fn      SampleApp_HandleKeys
 *
 * @brief   Handles all key events for this device.
 *
 * @param   shift - true if in shift/alt.
 * @param   keys - bit field for key events. Valid entries:
 *                 HAL_KEY_SW_2
 *                 HAL_KEY_SW_1
 *
 * @return  none
 */
void SampleApp_HandleKeys( uint8 shift, uint8 keys )
{
	(void)shift;  // Intentionally unreferenced parameter
 
}

/*********************************************************************
 * LOCAL FUNCTIONS
 */

/*********************************************************************
 * @fn      SampleApp_MessageMSGCB
 *
 * @brief   Data message processor callback.  This function processes
 *          any incoming data - probably from other devices.  So, based
 *          on cluster ID, perform the intended action.
 *
 * @param   none
 *
 * @return  none
 */
void SampleApp_MessageMSGCB( afIncomingMSGPacket_t *pkt )
{
#ifdef	WXL_COORD
	
	
	memcpy(RfRx.RxBuf,pkt->cmd.Data,32);//读出无线按收到的数据
	
	osal_stop_timerEx( SampleApp_TaskID, SAMPLEAPP_SEND_PERIODIC_MSG_EVT);//停止超时计数器
	HalLcdWriteString( (char*)"test", HAL_LCD_LINE_3 );
	
	if((RfRx.RXDATA.Head == '&') && (RfRx.RXDATA.LastByte == '*'))
	{
		memcpy(UartTxBuf.TxBuf,RfRx.RxBuf,32);
		
		for(i=0; i<8; i++)
		{
			UartTxBuf.TXDATA.Laddr[i] = RfRx.RXDATA.Laddr[i];//长地址
		}
		for(i=0; i<2; i++)
		{
			UartTxBuf.TXDATA.Saddr[i] = RfRx.RXDATA.Saddr[1-i];//短地址
		}
		UartTxBuf.TXDATA.CRC = CheckUartData(&UartTxBuf.TxBuf[1],29);

		HalUARTWrite ( HAL_UART_PORT_0, UartTxBuf.TxBuf, 32);//从串口输出
	}	
#endif
	
	
#if (defined(WXL_ROUTER) || defined(WXL_RFD))//ROUTER OR RFD
	
        ;
#endif	
}







/*********************************************************************
*********************************************************************/

