#ifndef  __GAS_H
#define  __GAS_H

extern uint16 HalAdcRead (uint8 channel, uint8 resolution);
extern INT8U ReadSensorAdc(INT8U channel);
extern void GasInit (void);

#endif
