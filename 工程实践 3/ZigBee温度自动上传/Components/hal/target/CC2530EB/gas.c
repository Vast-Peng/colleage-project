#include "stdio.h"
#include "string.h"

#include "ioCC2530.h"
#include "hal_types.h"
#include "ugOled9616.h"
#include "font.h"
#include "LcdDisp.h"
#include "gas.h"
#include "hal_adc.h"

#define IO_DIR_PORT_PIN1(port, pin, dir)  \
   do {                                  \
      if (dir == IO_OUT)                 \
         P##port##DIR |= (0x01<<(pin));  \
      else                               \
         P##port##DIR &= ~(0x01<<(pin)); \
   }while(0)

// Where port={0,1,2}, pin={0,..,7} and dir is one of:
#define IO_IN   0
#define IO_OUT  1


//----------------------------------------------------------
//Read Sensor ADC value
//读取AD值
//输入：通道名
//返回：8位AD值
//----------------------------------------------------------
INT8U ReadSensorAdc(INT8U channel)
{
	INT8U temp;
        temp = HalAdcRead(channel,01);
	return temp;
}


void GasInit (void)
{
  HalAdcInit ();
  IO_DIR_PORT_PIN1(0, 0, IO_IN);//ADC IN
  P0INP |= 0x01;
  IO_DIR_PORT_PIN1(1, 4, IO_OUT);//SLEEP
  P1_4 = 1;//打开可燃气体电源
  LcdPutString16_8(0, 0,  (void*)"     Gas    ", 12 , 1);
}

