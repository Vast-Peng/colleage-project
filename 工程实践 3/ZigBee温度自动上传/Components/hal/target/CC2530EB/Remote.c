#include "hal_types.h"
#include "hal_mcu.h"
#include "hal_uart.h"
#include "Remote.h"





/*******************************************************************************
* 函数名: void Remote_Con(uint8 n, uint8 dir)
* 功能介绍  : 红外控制函数
* 输入      : dat:控制数据)
* 输出      : None
* 返回      : 无
*******************************************************************************/
void Remote_Con(uint8 dat)
{
	uint8 arr[10];
	
	arr[0] = 0x68;
	arr[1] = 0xAA;
	arr[2] = 0x88;
	arr[3] = 0x01;
	arr[4] = dat;
	arr[5] = 0xCC;
								
	HalUARTWrite ( HAL_UART_PORT_0, arr, 6);//从串口输出
}


/*******************************************************************************
* 函数名: void Remote_Setting(uint8 n, uint8 dat)
* 功能介绍  : 红外设置函数
* 输入      :dat:控制数据)
* 输出      : None
* 返回      : 无
*******************************************************************************/
void Remote_Setting(uint8 dat)
{
	uint8 arr[10];
	
	arr[0] = 0x68;
	arr[1] = 0xAA;
	arr[2] = 0x80;
	arr[3] = 0x01;
	arr[4] = dat;
	arr[5] = 0xCC;
								
	HalUARTWrite ( HAL_UART_PORT_0, arr, 6);//从串口输出
}