#include "hal_types.h"
#include "hal_mcu.h"
#include "hal_uart.h"

void DAC_OUT_CON(uint16 DAC0, uint16 DAC1);

/*******************************************************************************
* 函数名: void MOTOR_Con(uint8 n, uint8 dir)
* 功能介绍  : 电机控制函数
* 输入      : n电机号，dir转动方向(0x80：正转    0x88：反转     0x8F：停止)
* 输出      : None
* 返回      : 无
*******************************************************************************/
void DAC_OUT_CON(uint16 DAC0, uint16 DAC1)
{
	uint8 arr[10];
	
	arr[0] = 0x68;
	arr[1] = 0xAA;
	arr[2] = 0xBF;
	arr[3] = 0x04;
	arr[4] = (uint8)(DAC0>>8);
	arr[5] = (uint8)DAC0;
	arr[6] = (uint8)(DAC1>>8);
	arr[7] = (uint8)DAC1;
	arr[8] = 0xCC;
								
	HalUARTWrite ( HAL_UART_PORT_0, arr, 9);//从串口输出
}