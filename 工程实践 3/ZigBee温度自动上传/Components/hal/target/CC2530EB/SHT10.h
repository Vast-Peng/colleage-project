#ifndef  __SHT10_H
#define  __SHT10_H

#include "hal_types.h"
#include "hal_defs.h"

#define NOP1 st( asm("NOP"); asm("NOP"); asm("NOP"); \
                                    asm("NOP"); asm("NOP"); asm("NOP"); \
                                    asm("NOP"); asm("NOP"); asm("NOP"); \
                                    asm("NOP"); asm("NOP"); asm("NOP"); \
                                    asm("NOP"); asm("NOP"); asm("NOP"); \
                                    asm("NOP"); asm("NOP"); asm("NOP"); \
                                    asm("NOP"); asm("NOP"); asm("NOP"); \
                                    asm("NOP"); asm("NOP"); asm("NOP"); )
#define NOP() st(NOP1;NOP1;NOP1;NOP1;NOP1;NOP1;NOP1;NOP1;NOP1;)

//管脚相关定义
#define SHT1X_DAT P1_4
#define SHT1X_DAT_DIR P1DIR
#define SHT1X_DAT_BV  BV(4)

#define SHT1X_SCK P1_5
#define SHT1X_SCK_DIR P1DIR
#define SHT1X_SCK_BV  BV(5)

#define SHT1X_DATA_IN   st(SHT1X_DAT_DIR &= ~(SHT1X_DAT_BV);)
#define SHT1X_DATA_OUT  st(SHT1X_DAT_DIR |= SHT1X_DAT_BV;)

extern void   SHT1X_PORT_INT(void);
extern uint8  WaitForSHT1XAck(void);
extern void   SHT1X_REST(void);
extern void   SHT1X_STAT(void);
extern uint8  SendToSHT1X(uint8 data);
extern void   WaitForAminit(uint16 delay);
extern void   SHT1X_INT(void);
extern uint16 Read_SHT1X(uint8 option);

#endif