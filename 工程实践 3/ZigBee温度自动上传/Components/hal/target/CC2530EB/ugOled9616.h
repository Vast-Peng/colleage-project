////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************************************************//
//                                                                                                //
//                                    by SU @ CD.china			          			                      //
//                                                                                                //
//                                   2010-08-18 15:37:30                                           //
//                                                                                                //
//------------------------------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////

//作		者：Sxl
//编写时间：2010-08-18 15:37:32
//版	  本：V1.0
//声		明：原创作品版权所有，违者必纠，程序仅供学习；商用，转载，请联系作者su_tech@126.com
//说		明：
//功		能: unvi 96*16 单色(蓝)OLED驱动；
//修改时间：
//修改备注:



#ifndef ugOled9616_h
#define ugOled9616_h

#include <ioCC2530.h> 				//包含相应的头文件

#ifndef BV
	#define BV(x) 	(1<<x)
#endif

#ifndef false
	#define false 0
#endif

#ifndef true
	#define true 1
#endif

#ifndef NULL
	#define NULL	0
#endif


typedef unsigned char OledDataType;
typedef const __code unsigned char  OledCodeDataType;
//SDA
#define OLED_SDA_PORT				P1
#define OLED_SDA_DIR				P1DIR
#define OLED_SDA_SEL				P1SEL
#define OLED_SDA_BIT				BV(2)
#define OLED_SDA_HIGH()			OLED_SDA_PORT |= OLED_SDA_BIT
#define OLED_SDA_LOW()			OLED_SDA_PORT &= ~OLED_SDA_BIT
//SCL
#define OLED_SCL_PORT				P1
#define OLED_SCL_DIR				P1DIR
#define OLED_SCL_SEL				P1SEL
#define OLED_SCL_BIT				BV(3)
#define OLED_SCL_HIGH()			OLED_SCL_PORT |= OLED_SCL_BIT
#define OLED_SCL_LOW()			OLED_SCL_PORT &= ~OLED_SCL_BIT
//

#define OLED_RES  false

#define OLED_RES_PORT				P0
#define OLED_RES_DIR				P0DIR
#define OLED_RES_SEL				P0SEL
#define OLED_RES_BIT				BV(2)
#define OLED_RES_HIGH()			OLED_RES_PORT |= OLED_RES_BIT
#define OLED_RES_LOW()			OLED_RES_PORT &= ~OLED_RES_BIT


#define OLED_PWR_PORT				P1
#define OLED_PWR_DIR				P1DIR
#define OLED_PWR_SEL				P1SEL
#define OLED_PWR_BIT				BV(2)
#define OLED_PWR_HIGH()			//OLED_PWR_PORT |= OLED_PWR_BIT
#define OLED_PWR_LOW()			//OLED_PWR_PORT &= ~OLED_PWR_BIT
#define OLED_PWR_OFF()			//do{ OLED_PWR_DIR &= ~OLED_PWR_BIT; }while(0)//OLED_PWR_PORT |= OLED_PWR_BIT
#define OLED_PWR_ON()			//  do{OLED_PWR_DIR |= OLED_PWR_BIT;OLED_PWR_LOW(); }while(0)//OLED_PWR_PORT &= ~OLED_PWR_BIT


extern void Set_Start_Column(OledDataType d);
extern void Set_Addressing_Mode(OledDataType d);
extern void Set_Column_Address(OledDataType a, OledDataType b);
extern void Set_Page_Address(OledDataType a, OledDataType b);
extern void Set_Start_Line(OledDataType d);
extern void Set_Contrast_Control(OledDataType d);
extern void Set_Charge_Pump(OledDataType d);
extern void Set_Segment_Remap(OledDataType d);
extern void Set_Entire_Display(OledDataType d);
extern void Set_Inverse_Display(OledDataType d);
extern void Set_Multiplex_Ratio(OledDataType d);
extern void Set_Display_On_Off(OledDataType d);
extern void Set_Start_Page(OledDataType d);
extern void Set_Common_Remap(OledDataType d);
extern void Set_Display_Offset(OledDataType d);
extern void Set_Display_Clock(OledDataType d);
extern void Set_Power_Save(OledDataType d);
extern void Set_Precharge_Period(OledDataType d);
extern void Set_Common_Config(OledDataType d);
extern void Set_VCOMH(OledDataType d);
extern void Set_NOP( void );

extern void Deactivate_Scroll( void );
extern void Horizontal_Scroll( OledDataType op, OledDataType startpag, 
						OledDataType stoppag, OledDataType step, OledDataType delay );
extern void Continuous_Scroll( OledDataType op, OledDataType StartRow, OledDataType StopRow, 
						OledDataType startline, OledDataType stopline, OledDataType row, OledDataType g );
extern void Vertical_Scroll(OledDataType a, OledDataType b, 
						OledDataType c, OledDataType d, OledDataType e);
extern void Fade_In( void );	
extern void Fade_Out( void );		
extern void OledSleep(unsigned char a);
extern void putChar( OledDataType x,OledDataType y,OledDataType dat );
extern void PutPic( OledDataType *ptr );						

extern void FillRam( OledDataType dat );
extern void ugOledClosePort( void );
extern void ugOled9616int( void );

extern void OledDelay( OledDataType ch );

#endif














