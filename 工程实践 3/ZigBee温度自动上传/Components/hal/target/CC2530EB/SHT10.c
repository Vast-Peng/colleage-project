#include "hal_types.h"
#include "hal_mcu.h"
#include "SHT10.h"


//温湿度传感器初始化
void SHT1X_PORT_INT(void)
{
	SHT1X_SCK_DIR |= SHT1X_SCK_BV;//时钟为输出
	SHT1X_DATA_OUT;	
	SHT1X_SCK = 0;
	SHT1X_DAT = 1;
}

//I2C等待应答
uint8 WaitForSHT1XAck(void)
{
	uint8 bc = 0;
	uint16 i = 10000;
	 
	SHT1X_DATA_IN;
	SHT1X_SCK = 0;
	NOP();
	NOP();
	
	do{
        if(SHT1X_DAT == 0)
		break;
		}while(i--);	
	SHT1X_SCK = 1;
	
	NOP();
	
	bc = SHT1X_DAT;
	SHT1X_DATA_OUT;
	SHT1X_SCK = 0;
	NOP();
	
	return bc;
}

//复位温湿度传感器
void SHT1X_REST(void)
{
	uint8 i;
	for(i = 0;i<9;i++)
	{
		SHT1X_SCK = 1;
		NOP();
		SHT1X_SCK = 0;
		NOP();
	}
	SHT1X_STAT();
}

//i2c收发
uint8 SendToSHT1X(uint8 data)
{
	uint8 i;
	SHT1X_SCK = 0;
	for(i = 0;i<8;i++)
	{
		if(data&0x80)
		{
			SHT1X_DAT = 1;
		}
		else
		{
			SHT1X_DAT = 0;
		}
		data <<= 1;
		SHT1X_SCK = 1;
		NOP();
		SHT1X_SCK = 0;
		NOP();
	}
	if(WaitForSHT1XAck() == 0)
	return 1;
	else
	return 0;
}

//状态转换
void SHT1X_STAT(void)
{
	SHT1X_DATA_OUT;
	SHT1X_SCK = 1;
	NOP();
	NOP();
	SHT1X_DAT = 0;		
	NOP();
	SHT1X_SCK = 0;	
	NOP();
	SHT1X_SCK = 1;
	NOP();
	SHT1X_DAT = 1;
	NOP();
	NOP();
	SHT1X_SCK = 0;	
}

//设置初始化
void SHT1X_INT(void)
{
	SHT1X_PORT_INT();
	SHT1X_REST();
}

//等待转换
void WaitForAminit(uint16 delay)
{
	do{;}while(delay--);
}

//读取温湿度
//输入：3-温度 5-湿度
//输出：读取的温湿度，浮点值
uint16 Read_SHT1X(uint8 option)
{
	uint16 temp = 0;
	uint8 i;
	
	SHT1X_STAT();
	
	if(SendToSHT1X(option&0x1f) == 0)return 1;//芯片没有应答
	SHT1X_DATA_IN;
			
	for(i = 0;i<10;i++)//等转换完成
	WaitForAminit(30000);	
	
	SHT1X_SCK = 0;
	NOP();
	SHT1X_SCK = 1;
	NOP();
	
	if(SHT1X_DAT != 0)
	{
		return 2;//芯片没有响应 转换完成
	}
	
	SHT1X_SCK = 0;
	NOP();
	
	for(i = 0;i<7;i++)
	{
		SHT1X_SCK = 1;
		NOP();
		temp <<= 1;
		if(SHT1X_DAT)
		{
			temp |= 1;
		}		
		SHT1X_SCK = 0;
		NOP();
	}		
	SHT1X_DATA_OUT;
	NOP();		
	SHT1X_DAT = 0;	
	NOP();	
	SHT1X_SCK = 1;
	NOP();NOP();
	SHT1X_SCK = 0;
	NOP();		
	NOP();
	SHT1X_DATA_IN;	
		
	for(i = 0;i<8;i++)
	{
		SHT1X_SCK = 1;
		NOP();
		temp <<= 1;
		if(SHT1X_DAT)
		{
			temp |= 1;
		}		
		SHT1X_SCK = 0;
		NOP();
	}
		
	SHT1X_DATA_OUT;
	NOP();		
	SHT1X_DAT = 1;		
	SHT1X_SCK = 1;
	NOP();
	SHT1X_SCK = 0;
	
	return temp;	
}