#include "hal_types.h"
#include "hal_mcu.h"

INT8U LEDDispWm = 1;//位码
INT16U LEDDispNum = 1234;//显示数据
INT8U LEDDispBuf[4];//显示绶冲区


void LED8MFWmSel(void);
void LEDDISPFUN(void);

__code const INT8U DispBMP[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f, 0x00};
                              //0   1    2    3    4    5    6    7    8    9    ' '



void LED8MFWmSel(void)
{
	switch(LEDDispWm)
	{
		case 0x03:
			P0 |= 0x08;
			break;
		case 0x02:
			P0 |= 0x04;
			break;
		case 0x01:
			P0 |= 0x02;
			break;
		case 0x00:
			P0 |= 0x01;
			break;
	}
}             




void LEDDISPFUN(void)
{
	INT16U temp;
	
	LEDDispBuf[0] = LEDDispNum/1000;
	
	temp = LEDDispNum%1000;
	
	LEDDispBuf[1] = temp/100;
	
	temp = temp%100;
	
	LEDDispBuf[2] = temp/10;
	
	LEDDispBuf[3] = temp%10;
}     