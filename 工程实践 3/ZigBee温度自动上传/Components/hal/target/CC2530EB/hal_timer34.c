



////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************************************************//
//                                                                                                //
//                                    by SU @ CD.china			          			                      //
//                                                                                                //
//                                   2010-7-19 14:42:14                                           //
//                                                                                                //
//------------------------------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////

//作		者：Sxl
//编写时间：2010-7-19 14:42:16
//版	  本：V1.0
//说		明：原创作品版权所有，违者必纠，程序仅供学习；商用，转载，请联系作者su_tech@126.com
//功		能: CC2530定时器34功能与配置 1比较输出  
//使用说明：
/*
		
*/
//使用证明：1.TIMER3比较输出到P16功能by Sutech;


#include "hal_timer34.h"

#if defined(TIMER3_EN) && (TIMER3_EN == ENABLE)
	#if defined( TIMER3_CHANNEL0_EN ) && ( TIMER3_CHANNEL0_EN == ENABLE )
		static void timer3Channel0Int( void );
	#endif
	
	#if defined( TIMER3_CHANNEL1_EN ) && ( TIMER3_CHANNEL1_EN == ENABLE )
		static void timer3Channel1Int( void );
	#endif
	static void Timer3int( void );
	
	#if defined( TIMER3_INTERRUPT_EN ) && ( TIMER3_INTERRUPT_EN == ENABLE )
		__interrupt void TIMER3_ISR(void);
	#endif
#endif




/*****************************************TIMER3 START*********************************************/
#if defined(TIMER3_EN) && (TIMER3_EN == ENABLE)
	
	#if defined( TIMER3_CHANNEL0_EN ) && ( TIMER3_CHANNEL0_EN == ENABLE )
	static void timer3Channel0Int( void )
	{		
		#if defined( TIMER3_CHANNEL0_INTERRUPT_EN ) && ( TIMER3_CHANNEL0_INTERRUPT_EN == ENABLE )
			T3CCTL0 |= TIMER34_CH_INTERRUPT_EN;
		#else
			T3CCTL0 &= ~TIMER34_CH_INTERRUPT_EN;
		#endif
		#if defined( TIMER3_CHANNEL0_CMP_OUT_EN ) && ( TIMER3_CHANNEL0_CMP_OUT_EN == ENABLE ) //比较功能
			T3CCTL0 = (T3CCTL0&(~TIMER34_CH_CMP_OUT_BIT)) | TIMER3_CHANNEL0_CMP_MODE|TIMER34_CH_MODE_BIT;
			T3CC0 = TIMER3_CHANNEL0_CMP_VAL;																										//存比较值
		#else 			//捕获功能
		
		#endif		
		
	}
	#endif
	
	#if defined( TIMER3_CHANNEL1_EN ) && ( TIMER3_CHANNEL1_EN == ENABLE )
	static void timer3Channel1Int( void )
	{
		;
	}
	#endif




	static void Timer3int( void )
	{
		T3CTL &= ~TIMER34_TICK_BIT;//清除分频位
		T3CTL |= TIMER3_TICK;//置分频系数
		T3CTL &= ~TIMER34_OVFIM;//关溢出中断
		//中断是否开启
		#if defined(TIMER3_INTERRUPT_EN)&&(TIMER3_INTERRUPT_EN == ENABLE)
			T3CTL |= TIMER34_OVFIM; //溢出中断开启
		#endif
		T3CTL = (T3CTL&(~TIMER34_MODE_BIT))|TIMER3_MODE;  //设置定时器工作模式	
		
				
		#if defined(TIMER3_CHANNEL0_EN) && (TIMER3_CHANNEL0_EN == ENABLE)
	 		timer3Channel0Int();
	 	#endif	
	 	#if defined(TIMER3_CHANNEL1_EN) && (TIMER3_CHANNEL1_EN == ENABLE)
	 		timer3Channel1Int();
	 	#endif	
	 	
			
	}
	#if defined( TIMER3_INTERRUPT_EN ) && ( TIMER3_INTERRUPT_EN == ENABLE )
	#pragma vector = T3_VECTOR
	__interrupt void TIMER3_ISR(void){
 		if( TIMIF & 0x01 )				//T3OVFIF溢出中断
 		{
 			TIMIF &= ~0x01;
 		}
 		if( TIMIF & 0x02 )		//T3CH0IF
 		{
 			TIMIF &= ~0x02
 		}
 		if( TIMIF & 0x04 )		//T3CH1IF
 		{
 			TIMIF &= ~0x04
 		}
	}	
	#endif	
#endif





/******************************************TIMER3 END**********************************************/



#if defined(TIMER4_EN) && (TIMER4_EN == ENABLE)
void Timer4int( void )
{
	;
}
#endif







void Timer34int( void )
{
#if defined(TIMER3_EN) && (TIMER3_EN == ENABLE)
	Timer3int();
#endif	

#if defined(TIMER4_EN) && (TIMER4_EN == ENABLE)
	Timer4int();
#endif	
}

void ChangT3Cmp0Val( unsigned char ch )
{
	Timer3Stop();
	T3CC0 = ch;
	Timer3Start();
}







