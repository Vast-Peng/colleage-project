#include <stdio.h>
#include <string.h>
#include "sensor.h"
#include "hal_types.h"
#include "hal_assert.h"
#include "hal_board.h"
#include "hal_defs.h"
#if defined( HAL_UART_DMA ) && HAL_UART_DMA
  #include "hal_dma.h"
#endif
#include "hal_mcu.h"
#include "hal_uart.h"
#include "osal.h"
#include "hal_adc.h"
#include "TMP006.h"
#include "hal_timer34.h"
#include "ugOled9616.h"

unsigned char SensorNum;
uint8 ParitySet;



void InitSensorIO(void);
void init_T3(void);


//******************************************************************************
//函数名：void init_T3(void)
//输入：无
//输出：无
//功能描述：初始化定时器3
//******************************************************************************
void init_T3(void)
{
	TIMER34_INIT(3);                  //初始化T3
	TIMER34_ENABLE_OVERFLOW_INT(3,1);  //开T3中断
	T3IE = 1;
        
	TIMER3_SET_CLOCK_DIVIDE(128);//时钟128分频101
	TIMER3_SET_MODE(0);                 //自动重装00－>0xff
	TIMER3_START(1);                    //启动
}



//--------------------------------------------------------
//Initialization I/O for sensor
// IO口初始化
//--------------------------------------------------------
void InitSensorIO(void)
{
        
       // Value = ReadSensorAdc(5);//采集SEL电压
        
        ugOled9616int();

        IO_DIR_PORT_PIN1(1, 6, IO_OUT);//BEEP
        P1_6 = 1;//OFF
        
        IO_DIR_PORT_PIN1(1, 4, IO_OUT);//CS_TC77
        IO_DIR_PORT_PIN1(1, 5, IO_OUT);//SCK_TC77
        IO_DIR_PORT_PIN1(1, 7, IO_IN);//DAT_TC77
        
        IO_DIR_PORT_PIN1(0, 1, IO_IN);//Lingt
        P0INP |= 0x02;
                                
        CS_TC77 = 1;
		
	
}

