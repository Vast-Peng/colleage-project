#include "hal_types.h"
#include "hal_mcu.h"
#include "TC77.h"




/****************************************************
函数名:INT8U Read Tc77(void)
功能：度温度传感器
输入：无
返回：温度值
****************************************************/
INT8U ReadTc77(void)
{
	INT16U temp=0;
	INT8U i;
        P1DIR &= ~0x80;
        MISO = 1;
        SCK = 0;
	CS_TC77 = 0;
	
	for(i=0; i<16; i++)
	{
		temp <<= 1;
		SCK = 1;
		asm("nop");
		if(MISO)temp++;
		SCK = 0;
		asm("nop");
	}
	CS_TC77 = 1;
        i = temp >> 7;
	return i;
}

/****************************************************
函数名:INT8U WriteTc77 Tc77(void)
功能：TC77休眠唤醒
输入：1-唤醒 0-休眠
返回：温度值
****************************************************/
INT8U WriteTc77(INT8U cfg)
{
   	INT16U temp = 0;
	BYTE i;
	
	SCK = 0;
	CS_TC77 = 0;

	for(i=0; i<16; i++)
	{
		temp <<= 1;
		SCK = 1;
	        asm("nop");    //nop
		if(MISO)temp++;
		SCK = 0;
		asm("nop");  //nop
	}

	P1DIR |= 0x80;

	for(i=0; i<16; i++)
	{
		asm("nop");    //nop
		if(cfg)
		{
			MISO = 0;	
		}
		else
		{
			MISO = 1;
		}
		SCK = 1;
		asm("nop");  //nop
		SCK = 0;
	}

	CS_TC77 = 1;
	P1DIR &= ~0x80;
	MISO = 1;
	i = temp >> 7;
	return i;
}