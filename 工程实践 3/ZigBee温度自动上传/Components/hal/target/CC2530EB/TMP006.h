#ifndef  __TMP006_H
#define  __TMP006_H

#include "hal_types.h"


void I2C_Delay(void);
void I2C_Start(void);
void I2C_Stop(void);
void I2C_NO_ACK(void);
void I2C_Ack(void);
uint8 I2C_Send8(uint8 IIC_data);
uint8 I2C_Send(uint8 IIC_data);
uint8 I2C_Read(void);
uint16 TMP006_Read(uint8 addr);

#endif