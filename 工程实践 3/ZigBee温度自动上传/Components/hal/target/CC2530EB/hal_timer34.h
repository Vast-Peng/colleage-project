

////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************************************************//
//                                                                                                //
//                                    by SU @ CD.china			          			                      //
//                                                                                                //
//                                   2010-7-19 14:42:14                                           //
//                                                                                                //
//------------------------------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////

//作		者：Sxl
//编写时间：2010-7-19 14:42:16
//版	  本：V1.0
//说		明：原创作品版权所有，违者必纠，程序仅供学习；商用，转载，请联系作者su_tech@126.com
//功		能: CC2530定时器34功能与配置   
//使用说明:	定时器34是一个8位定时器，可以用作定时捕获等功能，chnanel0只能输出固定频率，channel1在
//					channel0的配合下可以输出满足最大频率要求的任意频率
/*
						①: 始能相应定时器
						②: 定时器时钟频率						
						③: 定时器工作模式					
						④: 溢出中断是否开启
						⑤:	期望的中断时间
						⑥⑦⑧⑨⑩期望的中断时间
						
						
						溢出中断使用方式(定时器)：
							1.配置你需要的定时器，定时器3或,定时器4始能 "TIMERx_EN"
							2.配置你的定时器时钟 "TIMERx_TICK"
							3.配置你的定时器工作模式 "TIMERx_MODE"
							4.配置你需要的溢出中断始能位 "TIMERx_INTERRUPT_EN"
							5.配置你计算好的周期值,用于TxCC0计数		"TIMERx_COUNT_VOID"
						比较输出的使用方式
							1.配置你需要的定时器，定时器3或,定时器4始能 "TIMERx_EN"
							2.配置你的定时器时钟 "TIMERx_TICK"
							3.配置你的定时器工作模式 "TIMERx_MODE" 
							4.配置你的输出通道 "TIMERx_CHANNELx_EN"
							5.配置你选中通道的 "TIMERx_CHANNELx_CMP_OUT_EN"
							6.配置你选中通道的模式 "TIMERx_CHANNELx_CMP_MODE"
							7.配置你的比较值			"TIMERx_CHANNELx_CMP_VAL"
*/

#ifndef timer34_h
#define timer34_h

#include "ioCC2530.h"

#ifndef NULL
	#define NULL			0
#endif

#ifndef DISABLE
	#define DISABLE		0
#endif

#ifndef ENABLE
	#define ENABLE		1
#endif	


//TxCTL
//CLKCONCMD.TICKSPD 系统时钟32MHZ
#define TIMER34_TICK_BIT					0xE0
#define TIMER34_TICK_025MHZ    		0xE0
#define TIMER34_TICK_05MHZ    		0xC0
#define TIMER34_TICK_1MHZ			 		0xA0
#define TIMER34_TICK_2MHZ			 		0x80
#define TIMER34_TICK_4MHZ			 		0x60
#define TIMER34_TICK_8MHZ					0x40
#define TIMER34_TICK_16MHZ				0x20
#define TIMER34_TICK_32MHZ		 		0x00

#define TIMER34_RUN_START					0x10
#define TIMER34_OVFIM							0x08								//Overflow interrupt mask 0: Interrupt is disabled. 1: Interrupt is enabled.
#define TIMER34_CLR								0x04								/*Clear counter. Writing a 1 to CLR resets the counter to 0x00 and initialize all output pins of
																										associated channels. Always read as 0.*/
#define TIMER34_MODE_BIT					0x03
#define TIMER34_MODE_FREE_RUN			0x00								//自由运行 0x00->0xFF
#define TIMER34_MODE_DOWN					0x01
#define TIMER34_MODE_MODULO				0x02								//0x00->TxCC0
#define TIMER34_MODE_UP_DOWN			0x03								//0x00->TxCC0->0x00
//end of TxCTL

//start of TxCCTLx
#define TIMER34_CH_INTERRUPT_EN							0x40				//通道中断始能交叉开关
#define TIMER34_CH_CMP_OUT_BIT							0x38				//设置比较输出位
#define TIMER34_CH_CMP_OUT_SET							0x00				//输出高
#define TIMER34_CH_CMP_OUT_CLEAR						0x08				//时间到输出低
#define TIMER34_CH_CMP_OUT_TOGGLE						0x10				//取反
#define TIMER34_CH_CMP_OUT_H_CMP_L_FF				0X28				//101: Set output on compare, clear on 0xFF
#define TIMER34_CH_CMP_OUT_H_0							0x30				//输出从高到0;时间到输出为高TxCC0为0时输出0

#define TIMER34_CH_MODE_BIT									0x04				//0捕获模式,1比较模式
#define TIMER34_CH_CAP_BIT									0x03				//Capture mode select								
#define TIMER34_CH_CAP_NO_CAP								0x00				//No capture
#define TIMER34_CH_CAP_RISING_EDGE					0x01				//Capture on rising edge
#define TIMER34_CH_CAP_FAILLING_EDGE				0x02				//Capture on falling edge
#define TIMER34_CH_CAP_BOTH_EDGES						0x03				//Capture on both edges
//end of TxCCTLx  Timer x Channel x Capture/Compare Value

//start TxCCx
/*
Timer capture/compare value channel 0. Writing to this register when TxCCTL0.MODE=1 (compare
mode) causes the TxCC0.VAL[7:0] update to the written value to be delayed until
TxCNT.CNT[7:0]=0x00.
*/
//end if TxCCx



//这里作一些全局的定义
//timer3 global start
#define TIMER3_EN								ENABLE							//①: 始能相应定时器
#define TIMER3_TICK							TIMER34_TICK_025MHZ		//②: 定时器时钟频率
#define TIMER3_MODE 						TIMER34_MODE_FREE_RUN	//③: 定时器工作模式
#define TIMER3_INTERRUPT_EN			DISABLE							//④: 溢出中断是否开启
#define TIMER3_COUNT_VOID				255									//⑤:	期望的中断时间 需要跟据定时方式自己运算
//channal x     通道始能位
#define TIMER3_CHANNEL0_EN						ENABLE				//Alt1:P13;Alt2:P16
#define TIMER3_CHANNEL0_INTERRUPT_EN 	DISABLE				//通道0中断始能位
#define TIMER3_CHANNEL0_CMP_OUT_EN		ENABLE				//比较输出
#define TIMER3_CHANNEL0_CMP_MODE			TIMER34_CH_CMP_OUT_H_CMP_L_FF   //比较器模式
#define TIMER3_CHANNEL0_CMP_VAL				104						//比较值

#define TIMER3_CHANNEL1_EN						DISABLE				//Alt1:P14;Alt2:P17
#define TIMER3_CHANNEL1_INTERRUPT_EN 	DISABLE				//通道1中断始能位


//timer3 不需要定义的函数
#define Timer3Start()				do{T3CTL |= TIMER34_RUN_START;}while(0)			
#define Timer3Stop()				do{T3CTL &= ~TIMER34_RUN_START;}while(0)
#define Timer3Clear()				do{T3CTL |= TIMER34_CLR;}while(0)


//timer3 global end

//timer4
#define TIMER4_EN											DISABLE



//timer4 不需要定义的函数
#define Timer4Start()				do{T4CTL |= TIMER34_RUN_START;}while(0)			
#define Timer4Stop()				do{T4CTL &= ~TIMER34_RUN_START;}while(0)
#define Timer4Clear()				do{T4CTL |= TIMER34_CLR;}while(0)


extern void Timer34int( void );
extern void ChangT3Cmp0Val( unsigned char ch );

#endif
















