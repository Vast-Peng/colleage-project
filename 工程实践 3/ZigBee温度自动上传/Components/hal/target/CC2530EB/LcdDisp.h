


////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************************************************//
//                                                                                                //
//                                    by SU @ CD.china			          			                      //
//                                                                                                //
//                                   2010-08-20 15:00:31                                          //
//                                                                                                //
//------------------------------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////

//作		者：Sxl
//编写时间：2010-08-20 15:00:28
//版	  本：V1.0
//声		明：原创作品版权所有，违者必纠，程序仅供学习；商用，转载，请联系作者su_tech@126.com
//说		明：
//功		能: unvi 96*16 单色(蓝)OLED驱动；
//修改时间：
//修改备注:





#ifndef LcdDisp_h
#define LcdDisp_h

#include "hal_types.h"
//#include "typedef.h"    //数据类型定义头文件
#include "ugOled9616.h"

#define FONTLISTCOUNT  				94//(sizeof(fontList)/sizeof(fontList[0]))
#define fontChar16ListCount 	118//汉字和字数

#define RIGHT_SC		0			//向右滑
#define LIFT_SC			1			//向左滑

extern void LcdClearRam( void );
extern void LcdClearScrean( void );

extern void LcdPutString16_8( u8 x,u8 y,u8 *ptr,u8 len,u8 op );
extern void LcdPutString16_16( u8 x,u8 y,u8 *ptr,u8 len,u8 op );
extern void LcdPutDispBuf( u8 x,u8 y,OledCodeDataType *ptr,u8 op );
extern void LcdPutDisp( u8 x,u8 y,OledCodeDataType *ptr,u8 op );
extern void LcdPutPic( u8 x, u8 y,u8 w,u8 h,OledCodeDataType *ptr,u8 op );
extern void LcdPutScDispRtoL( OledCodeDataType *ptr,u8 op,u16 dl );
extern void LcdPutScDispRtoL12( OledCodeDataType *ptr,u8 op,u16 dl );
extern void LcdPutScDispLtoR12( OledCodeDataType *ptr,u8 op,u16 dl );
extern void LcdPutScString( OledCodeDataType *ptr,u8 op,u8 rl,u16 dl );
extern void LcdPutScPic(  OledCodeDataType *ptr, u8 op,u8 qp,u16 dl );

#endif










