#ifndef  __TC77_H
#define  __TC77_H

#include "hal_types.h"
#include "hal_defs.h"


#define CS_TC77         P1_4
#define SCK             P1_5
#define MISO            P1_7


INT8U ReadTc77(void);
INT8U WriteTc77(INT8U cfg);


#endif