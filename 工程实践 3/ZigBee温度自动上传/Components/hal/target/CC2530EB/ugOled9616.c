

////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************************************************//
//                                                                                                //
//                                    by SU @ CD.china			          			                      //
//                                                                                                //
//                                   2010-08-18 15:37:30                                           //
//                                                                                                //
//------------------------------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////

//作		者：Sxl
//编写时间：2010-08-18 15:37:32
//版	  本：V1.0
//声		明：原创作品版权所有，违者必纠，程序仅供学习；商用，转载，请联系作者su_tech@126.com
//说		明：
//功		能: unvi 96*16 单色(蓝)OLED驱动；
//修改时间：
//修改备注:

/*

===============================================------com15
|																							|
|																							|
|---------------------------------------------|
|																							|
|																							|
=============================================== -----com0
|																							|
row127																				row32

*/
#include "hal_types.h"
//#include "typedef.h"    //数据类型定义头文件
#include "ugOled9616.h"


#define XLevelL		0x00
#define XLevelH		0x10
#define XLevel		((XLevelH&0x0F)*16+XLevelL)
#define Max_Column	96
#define Max_Row			16
#define	Brightness	0x3F


static void ugOledPortInt( void );
static void uDelay(OledDataType l);
//static void Delay(OledDataType n);
static void I2C_O( OledDataType mcmd );
static void I2C_Ack( void );
//static void I2C_NAck( void );
static void I2C_Start( void );
static void I2C_Stop( void );
static void Write_Command(OledDataType Data);
//static void Write_Data(OledDataType Data);
static void OLED_Init( void );

void OledDelay( OledDataType ch )
{
	OledDataType i;
	for( i = 0;i < ch; i ++ )
	{
		uDelay(255);
	}
}

void ugOledClosePort( void )
{
	OLED_PWR_OFF();

	OLED_SDA_DIR &= ~OLED_SDA_BIT;					//input
	

	OLED_SCL_DIR &= ~OLED_SCL_BIT;					//input
	
#if	defined(OLED_RES) && (OLED_RES == true)
	OLED_RES_DIR &= ~OLED_RES_BIT;					//OutPut
#endif	
}

static void ugOledPortInt( void )
{	
	OLED_SDA_SEL &= ~OLED_SDA_BIT;				//General-purpose I/O
	OLED_SDA_DIR |= OLED_SDA_BIT;					//OutPut
	
	OLED_SCL_SEL &= ~OLED_SCL_BIT;				//General-purpose I/O
	OLED_SCL_DIR |= OLED_SCL_BIT;					//OutPut
	
#if	defined(OLED_RES) && (OLED_RES == true)
	OLED_RES_SEL &= ~OLED_RES_BIT;				//General-purpose I/O
	OLED_RES_DIR |= OLED_RES_BIT;					//OutPut
#endif	
	
	
	OLED_PWR_SEL &= ~OLED_PWR_BIT;				//General-purpose I/O
	OLED_PWR_DIR |= OLED_PWR_BIT;					//OutPut
	
	OLED_PWR_ON();
	OledDelay(80);
	OLED_SDA_HIGH();
	OLED_SCL_HIGH();
}

static void uDelay(OledDataType l)
{
	OledDataType i;
	for( i = 0;i < l;i ++ );	
}
/*static void Delay(OledDataType n)
{
	OledDataType i,j,k;
	for(k=0;k<n;k++)
	{
		for(i=0;i<131;i++)
		{
			for(j=0;j<15;j++)
			{
				uDelay(203);	
			}
		}
	}
}*/
static void I2C_O( OledDataType mcmd )
{
	OledDataType length = 8;			// Send Command
	while(length--)
	{
		if(mcmd & 0x80)
		{
			OLED_SDA_HIGH();
		}
		else
		{
			OLED_SDA_LOW();
		}
		uDelay(1);
		OLED_SCL_HIGH();
		uDelay(1);
		OLED_SCL_LOW();
		uDelay(1);
		mcmd = mcmd << 1;
	}
}
static void I2C_Ack()
{
	OLED_SDA_HIGH();
	uDelay(1);
	OLED_SCL_HIGH();
	uDelay(1);
	OLED_SCL_LOW();
	uDelay(1);
}/*
static void I2C_NAck()
{
	OLED_SDA_LOW();
	uDelay(1);
	OLED_SCL_HIGH();
	uDelay(1);
	OLED_SCL_LOW();
	uDelay(1);
}*/
static void I2C_Start()
{
	OLED_SDA_LOW();
	uDelay(1);
	OLED_SCL_HIGH();
	uDelay(1);
	OLED_SCL_LOW();
	uDelay(1);
	I2C_O(0x78);
	I2C_Ack();
}

static void I2C_Stop()
{
	OLED_SCL_HIGH();
	uDelay(5);
	OLED_SDA_LOW();
	uDelay(5);
	OLED_SDA_HIGH();
	uDelay(5);
}


static void Write_Command(OledDataType Data)
{
	I2C_Start();
	I2C_O(0x00);
	I2C_Ack();
	I2C_O(Data);
	I2C_Ack();
	I2C_Stop();
}

/*
static void Write_Data(OledDataType Data)
{
	I2C_Start();
	I2C_O(0x40);
	I2C_Ack();
	I2C_O(Data);
	I2C_Ack();
	I2C_Stop();
}
*/
//设置列地址 高底8位
void Set_Start_Column(OledDataType d)
{
	Write_Command(0x00+d%16);		// Set Lower Column Start Address for Page Addressing Mode
															// Default => 0x00
	Write_Command(0x10+d/16);		// Set Higher Column Start Address for Page Addressing Mode
															// Default => 0x10
}
//设置地址模式，一共有3种地址模式
void Set_Addressing_Mode(OledDataType d)
{
	Write_Command(0x20);			// Set Memory Addressing Mode
	Write_Command(d);			//   Default => 0x02
						//     0x00 => Horizontal Addressing Mode
						//     0x01 => Vertical Addressing Mode
						//     0x02 => Page Addressing Mode
}
//设置列地址
void Set_Column_Address(OledDataType a, OledDataType b)
{
	Write_Command(0x21);			// Set Column Address
	Write_Command(a);			//   Default => 0x00 (Column Start Address)
	Write_Command(b);			//   Default => 0x7F (Column End Address)
}
//页起始地址
void Set_Page_Address(OledDataType a, OledDataType b)
{
	Write_Command(0x22);			// Set Page Address
	Write_Command(a);			//   Default => 0x00 (Page Start Address)
	Write_Command(b);			//   Default => 0x07 (Page End Address)
}

//设置com0映射
void Set_Start_Line(OledDataType d)
{
	Write_Command(0x40|d);			// Set Display Start Line
						//   Default => 0x40 (0x00)
}
//设置对比度 范围可以0-255
void Set_Contrast_Control(OledDataType d)
{
	Write_Command(0x81);			// Set Contrast Control
	Write_Command(d);			//   Default => 0x7F
}

void Set_Charge_Pump(OledDataType d)
{
	Write_Command(0x8D);			// Set Charge Pump
	Write_Command(0x10|d);			//   Default => 0x10
						//     0x10 (0x00) => Disable Charge Pump
						//     0x14 (0x04) => Enable Charge Pump
}

void Set_Segment_Remap(OledDataType d)
{
	Write_Command(0xA0|d);			// Set Segment Re-Map
						//   Default => 0xA0
						//     0xA0 (0x00) => Column Address 0 Mapped to SEG0
						//     0xA1 (0x01) => Column Address 0 Mapped to SEG127
}


void Set_Entire_Display(OledDataType d)
{
	Write_Command(0xA4|d);			// Set Entire Display On / Off
						//   Default => 0xA4
						//     0xA4 (0x00) => Normal Display
						//     0xA5 (0x01) => Entire Display On
}


void Set_Inverse_Display(OledDataType d)
{
	Write_Command(0xA6|d);			// Set Inverse Display On/Off
						//   Default => 0xA6
						//     0xA6 (0x00) => Normal Display
						//     0xA7 (0x01) => Inverse Display On
}


void Set_Multiplex_Ratio(OledDataType d)
{
	Write_Command(0xA8);			// Set Multiplex Ratio
	Write_Command(d);			//   Default => 0x3F (1/64 Duty)
}


void Set_Display_On_Off(OledDataType d)	
{
	Write_Command(0xAE|d);			// Set Display On/Off
						//   Default => 0xAE
						//     0xAE (0x00) => Display Off
						//     0xAF (0x01) => Display On
}


void Set_Start_Page(OledDataType d)
{
	Write_Command(0xB0|d);			// Set Page Start Address for Page Addressing Mode
						//   Default => 0xB0 (0x00)
}


void Set_Common_Remap(OledDataType d)
{
	Write_Command(0xC0|d);			// Set COM Output Scan Direction
						//   Default => 0xC0
						//     0xC0 (0x00) => Scan from COM0 to 63
						//     0xC8 (0x08) => Scan from COM63 to 0
}


void Set_Display_Offset(OledDataType d)
{
	Write_Command(0xD3);			// Set Display Offset
	Write_Command(d);			//   Default => 0x00
}


void Set_Display_Clock(OledDataType d)
{
	Write_Command(0xD5);			// Set Display Clock Divide Ratio / Oscillator Frequency
	Write_Command(d);			//   Default => 0x80
						//     D[3:0] => Display Clock Divider
						//     D[7:4] => Oscillator Frequency
}



void Set_Power_Save(OledDataType d)
{
	Write_Command(0xD8);			// Set Low Power Display Mode
	Write_Command(d);			//   Default => 0x00 (Normal Power Display Mode)
}


void Set_Precharge_Period(OledDataType d)
{
	Write_Command(0xD9);			// Set Pre-Charge Period
	Write_Command(d);			//   Default => 0x22 (2 Display Clocks [Phase 2] / 2 Display Clocks [Phase 1])
						//     D[3:0] => Phase 1 Period in 1~15 Display Clocks
						//     D[7:4] => Phase 2 Period in 1~15 Display Clocks
}


void Set_Common_Config(OledDataType d)
{
	Write_Command(0xDA);			// Set COM Pins Hardware Configuration
	Write_Command(0x02|d);			//   Default => 0x12 (0x10)
						//     Alternative COM Pin Configuration
						//     Disable COM Left/Right Re-Map
}


void Set_VCOMH(OledDataType d)
{
	Write_Command(0xDB);			// Set VCOMH Deselect Level
	Write_Command(d);			//   Default => 0x20 (0.77*VCC)
}


void Set_NOP( void )
{
	Write_Command(0xE3);			// Command for No Operation
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Deactivate Scrolling (Full Screen) 解除所有移动
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Deactivate_Scroll( void )
{
	Write_Command(0x2E);			// Deactivate Scrolling
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Continuous Horizontal Scrolling (Partial or Full Screen)
//    水平移动功能,使用这命令之前需要使用2E命令
//    op: Scrolling Direction
//       "0x00" (Rightward)
//       "0x01" (Leftward)
//    startpag: Define Start Page Address
//    stoppag: Define End Page Address
//    step: Set Time Interval between Each Scroll Step in Terms of Frame Frequency 速度
//    delay: Delay Time
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Horizontal_Scroll( OledDataType op, OledDataType startpag, 
OledDataType stoppag, OledDataType step, OledDataType delay )
{
	Write_Command(0x26|op);			// Horizontal Scroll Setup
	Write_Command(0x00);			//           => (Dummy Write for First Parameter)
	Write_Command( startpag );
	Write_Command( step );
	Write_Command( stoppag );
	Write_Command(0x2F);			// Activate Scrolling
//	Delay(delay);
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Continuous Vertical / Horizontal / Diagonal Scrolling (Partial or Full Screen)
//
//    op: Scrolling Direction
//       "0x00" (Vertical & Rightward)
//       "0x01" (Vertical & Leftward)
//    StartRow: Define Start Row Address (Horizontal / Diagonal Scrolling)
//    StopRow: Define End Page Address (Horizontal / Diagonal Scrolling)
//    startline: Set Top Fixed Area (Vertical Scrolling) 开始行
//    stopline: Set Vertical Scroll Area (Vertical Scrolling)//结束行
//    row: Set Numbers of Row Scroll per Step (Vertical / Diagonal Scrolling) 列向运行多少点
//    g:移动速度 Set Time Interval between Each Scroll Step in Terms of Frame Frequency
//    * d+e must be less than or equal to the Multiplex Ratio...
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Continuous_Scroll( OledDataType op, OledDataType StartRow, OledDataType StopRow, 
OledDataType startline, OledDataType stopline, OledDataType row, OledDataType g )
{
	Write_Command(0xA3);			// Set Vertical Scroll Area
	Write_Command(startline);			//   Default => 0x00 (Top Fixed Area)
	Write_Command(stopline);			//   Default => 0x40 (Vertical Scroll Area)

	Write_Command(0x29|op);			// Continuous Vertical & Horizontal Scroll Setup
	Write_Command(0x00);			//           => (Dummy Write for First Parameter)
	Write_Command(StartRow);
	Write_Command(g);
	Write_Command(StopRow);
	Write_Command(row);
	Write_Command(0x2F);			// Activate Scrolling
	//Delay(h);
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Vertical / Fade Scrolling (Partial or Full Screen)
//
//    a: Scrolling Direction
//       "0x00" (Upward)
//       "0x01" (Downward)
//    b: Set Top Fixed Area
//    c: Set Vertical Scroll Area
//    d: Set Numbers of Row Scroll per Step
//    e: Set Time Interval between Each Scroll Step
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Vertical_Scroll(OledDataType a, OledDataType b, OledDataType c, OledDataType d, OledDataType e)
{
	unsigned int i,j;	

	Write_Command(0xA3);			// Set Vertical Scroll Area
	Write_Command(b);			//   Default => 0x00 (Top Fixed Area)
	Write_Command(c);			//   Default => 0x40 (Vertical Scroll Area)

	switch(a)
	{
		case 0:
			for(i=0;i<c;i+=d)
			{
				Set_Start_Line(i);
				for(j=0;j<e;j++)
				{
					uDelay(200);
				}
			}
			break;
		case 1:
			for(i=0;i<c;i+=d)
			{
				Set_Start_Line(c-i);
				for(j=0;j<e;j++)
				{
					uDelay(200);
				}
			}
			break;
	}
	Set_Start_Line(0x00);
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Fade In (Full Screen)  对比度操作
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Fade_In( void )
{
unsigned int i,j;	

	Set_Display_On_Off(0x01);
	for(i=0;i<(Brightness+1);i++)
	{
		Set_Contrast_Control(i);
		for( j = 0;j < 20;j ++)
		uDelay(200);		
	}
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Fade Out (Full Screen)对比度操作
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Fade_Out( void )
{
unsigned int i,j;	

	for(i=(Brightness+1);i>0;i--)
	{
		Set_Contrast_Control(i-1);
		for( j = 0;j < 20;j ++)
			uDelay(200);
	}
	Set_Display_On_Off(0x00);
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//  Sleep Mode
//
//    "0x00" Enter Sleep Mode
//    "0x01" Exit Sleep Mode
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void OledSleep(unsigned char a)
{
	switch(a)
	{
		case 0:
			Set_Display_On_Off(0x00);
			Set_Entire_Display(0x01);
			break;
		case 1:
			Set_Entire_Display(0x00);
			Set_Display_On_Off(0x01);
			break;
	}
}


void putChar( OledDataType x,OledDataType y,OledDataType dat )
{
	if( x > 128 )
		return;
	if( y > 64 )
		return;
	Set_Start_Page(y);
	Set_Start_Column(0x00);		
	I2C_Start();
	I2C_O(0x40);
	I2C_Ack();
	I2C_O( dat );
	I2C_Ack();
	I2C_Stop();
	
}

//全屏填充同一数据
void FillRam( OledDataType dat )
{
	OledDataType i,j;
	for(i=0;i<2;i++)
	{
		Set_Start_Page(i);
		Set_Start_Column(0x00);		
		I2C_Start();
		I2C_O(0x40);
		I2C_Ack();
		for(j=32;j<128;j++)
		{
			I2C_O( dat );
			I2C_Ack();
		}
		I2C_Stop();
	}
}

void PutPic( OledDataType *ptr )
{
	OledDataType i,j;
	for(i=0;i<2;i++)
	{
		Set_Start_Page(i);
		Set_Start_Column(0x00);		
		I2C_Start();
		I2C_O(0x40);
		I2C_Ack();
		for(j=32;j<128;j++)
		{
			I2C_O( *ptr++ );
			I2C_Ack();
		}
		I2C_Stop();
	};
}


static void OLED_Init()
{

#if	defined(OLED_RES) && (OLED_RES == true)
	OledDataType i;
	RES=0;
	for(i=0;i<20;i++)
	{
		uDelay(200);
	}
	RES=1;
#endif

	Set_Display_On_Off(0x00);		// Display Off (0x00/0x01)
	Set_Display_Clock(0xA2);		// Set Clock as 120 Frames/Sec
	Set_Multiplex_Ratio(0x0F);		// 1/16 Duty (0x0F~0x3F)
	Set_Display_Offset(0x00);		// Shift Mapping RAM Counter (0x00~0x3F)
	Set_Start_Line(0x00);			// Set Mapping RAM Display Start Line (0x00~0x3F)
	Set_Charge_Pump(0x04);			// Enable Embedded DC/DC Converter (0x00/0x04)
	Set_Power_Save(0x05);			// Set Low Power Save Mode
	Set_Addressing_Mode(0x02);		// Set Page Addressing Mode (0x00/0x01/0x02)
	Set_Segment_Remap(0x01);		// Set SEG/Column Mapping (0x00/0x01)
	Set_Common_Remap(0x08);			// Set COM/Row Scan Direction (0x00/0x08)
	Set_Common_Config(0x00);		// Set Sequential Configuration (0x00/0x10)
	Set_Contrast_Control(Brightness);	// Set SEG Output Current
	Set_Precharge_Period(0xD2);		// Set Pre-Charge as 13 Clocks & Discharge as 2 Clock
	Set_VCOMH(0x20);			// Set VCOM Deselect Level
	Set_Entire_Display(0x00);		// Disable Entire Display On (0x00/0x01)
	Set_Inverse_Display(0x00);		// Disable Inverse Display On (0x00/0x01)

	FillRam(0x00);				// Clear Screen

	Set_Display_On_Off(0x01);		// Display On (0x00/0x01)
}



void ugOled9616int( void )
{
	ugOledPortInt();
	OLED_Init();	
}





















































