#ifndef SENSOR_H
#define SENSOR_H

#include "hal_types.h"

#include "TC77.h"
/*
#include "LED8MF.h"
#include "Motor.h"
#include "Remote.h"
#include "SHT10.h"
*/

#define IO_DIR_PORT_PIN1(port, pin, dir)  \
   do {                                  \
      if (dir == IO_OUT)                 \
         P##port##DIR |= (0x01<<(pin));  \
      else                               \
         P##port##DIR &= ~(0x01<<(pin)); \
   }while(0)

// Where port={0,1,2}, pin={0,..,7} and dir is one of:
#define IO_IN   0
#define IO_OUT  1



#define		XOUT			0X00		//x轴
#define		YOUT			0X01		//y轴
#define		ZOUT			0X04		//z轴
#define		VPHOTO			0X00		//光敏


#define 	PJVCC           P2_0        

#define   	JVCCON()      PJVCC = 1;  //打开传感器电源
#define   	JVCCOFF()     PJVCC = 0;  //关闭传感器电源



BYTE halSetTimer34Period(BYTE timer, u32 period);


/*****************************************
//T3配置定义
*****************************************/
// Where _timer_ must be either 3 or 4
// Macro for initialising timer 3 or 4
//将T3/4配置寄存复位
#define TIMER34_INIT(timer)   \
   do {                       \
      T##timer##CTL   = 0x06; \
      T##timer##CCTL0 = 0x00; \
      T##timer##CC0   = 0x00; \
      T##timer##CCTL1 = 0x00; \
      T##timer##CC1   = 0x00; \
   } while (0)

//Macro for enabling overflow interrupt
//打开T3/4溢出中断
#define TIMER34_ENABLE_OVERFLOW_INT(timer,val) \
   (T##timer##CTL =  (val) ? T##timer##CTL | 0x08 : T##timer##CTL & ~0x08)




//启动T3
#define TIMER3_START(val)                         \
    (T3CTL = (val) ? T3CTL | 0X10 : T3CTL&~0X10)

//时钟分步选择
#define TIMER3_SET_CLOCK_DIVIDE(val)              \
  do{                                             \
    T3CTL &= ~0XE0;                               \
      (val==2) ? (T3CTL|=0X20):                   \
      (val==4) ? (T3CTL|=0x40):                   \
      (val==8) ? (T3CTL|=0X60):                   \
      (val==16)? (T3CTL|=0x80):                   \
      (val==32)? (T3CTL|=0xa0):                   \
      (val==64) ? (T3CTL|=0xc0):                  \
      (val==128) ? (T3CTL|=0XE0):                 \
      (T3CTL|=0X00);             /* 1 */          \
  }while(0)

//Macro for setting the mode of timer3
//设置T3的工作方式
#define TIMER3_SET_MODE(val)                      \
  do{                                             \
    T3CTL &= ~0X03;                               \
    (val==1)?(T3CTL|=0X01):  /*DOWN            */ \
    (val==2)?(T3CTL|=0X02):  /*Modulo          */ \
    (val==3)?(T3CTL|=0X03):  /*UP / DOWN       */ \
    (T3CTL|=0X00);           /*free runing */     \
  }while(0)



extern void Write595(INT8U dat);


extern void init_T3(void);
extern void InitSensorIO(void);
extern INT8U ReadSensorAdc(INT8U channel);
extern void DAC_OUT_CON(uint16 DAC0, uint16 DAC1);

#endif //SENSOR_H

