
////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************************************************//
//                                                                                                //
//                                    by SU @ CD.china			          			                      //
//                                                                                                //
//                                   2010-08-20 15:00:41                                           //
//                                                                                                //
//------------------------------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////

//作		者：Sxl
//编写时间：2010-08-20 15:00:38
//版	  本：V1.0
//声		明：原创作品版权所有，违者必纠，程序仅供学习；商用，转载，请联系作者su_tech@126.com
//说		明：
//功		能: unvi 96*16 单色(蓝)OLED驱动；注意：该驱动使用的整屏刷新模式,为了提高速度节省时间很多
//					函数里面没有对参数作判断使用时要谨慎
//修改时间：
//修改备注:

#include "hal_types.h"
#include "ugOled9616.h"
#include "font.h"
#include "LcdDisp.h"


#include "stdio.h"
#include "string.h"

static u8 LcdBuf[2][96];



static void LcdDelay( u16 d );
static u16 lookforChar( u8 ch );
static u16 lookforChar16( u16 ch );
static void LcdPrint8( u8 x,u8 y,u8 vl,u8 op );
static void LcdPrint16( u8 x, u8 y, u16 val, u8 op );



static void LcdDelay( u16 d )
{
	u16 i,j;
	for( i = 0 ;i < d; i ++ )
		for( j = 0 ;j < d; j ++ );
}

/**************************************************************************************************/
/*
功	能：查找字符在索引中的哪一个位置
*/
/**************************************************************************************************/
static u16 lookforChar( u8 ch )
{
	uint16 i;
	for( i = 0; i < FONTLISTCOUNT; i ++ )
	{
		if( fontList[i] == ch )
			return i;
	}
	return i;
}

//查中文
static u16 lookforChar16( u16 ch )
{
	uint16 i,j;
	u16 temp16;
	for( i = 0; i < fontChar16ListCount; i ++ )
	{
		j = i*2;
		temp16 = fontChar16List[j + 1];
		temp16 <<= 8;
		temp16 |= fontChar16List[j];
		if( temp16 == ch )
			return i;
	}
	return i;
}




/**************************************************************************************************/
/*
功	能：在指定位置输出8*8
*/
/**************************************************************************************************/
static void LcdPutChar8( u8 x,u8 y,u8 ch )
{
	LcdBuf[y][x] = ch;
}
/**************************************************************************************************/
/*
功	能：在指定位置输出16*16
*/
/**************************************************************************************************/

/*static void LcdPutChar16( u8 x,u8 y,u16 ch )
{
	LcdBuf[y][x] = (u8)ch;										//低前高后
	LcdBuf[y+1][x] = (u8)(ch>>8);
}

void LcdPutString8( u8 x,u8 y,u8 *ptr u8 len,u8 op )
{	
	u8 i,*tptr = ptr,xx = x,yy = y;
	u16 m;
	if( x > 95)
		return ;
	if( y > 1)
		return ;
	for( i = 0;i < len; i ++ )
	{
		m = lookforChar(*tptr ++);
		if( m != FONTLISTCOUNT )
			{
				m = m * 8;
			}
		else
			return;
		xx += 8;
		if( xx > 88 )
			return;
	}
}
*/


void LcdClearRam( void )
{
	u8 i;
	for( i = 0;i < 96;i ++ )
	{
		LcdBuf[0][i] = 0;
	}
	for( i = 0;i < 96;i ++ )
	{
		LcdBuf[1][i] = 0;
	}
}
void LcdClearScrean( void )
{
	LcdClearRam();
	PutPic( (void *)LcdBuf );
}

void LcdPutString16_8( u8 x,u8 y,u8 *ptr,u8 len,u8 op )
{
	u8 i,j,*tptr = ptr,xx = x,yy = y;
	u16 m;
	if( xx > 95)
		return ;
	if( yy )
		return ;	
	for( i = 0;i < len; i ++ )
	{
		m = lookforChar(*tptr ++);
		if( m != FONTLISTCOUNT )
		{
			m = m * 16;
			for( j = 0;j < 8;j ++ )
			{
				if(op)
				{
					LcdPutChar8( (xx + j),yy,font[m+j] );
					LcdPutChar8( (xx + j),yy+1,font[m+j+8] );
				}
				else
				{
					LcdPutChar8( (xx + j),yy,~font[m+j] );
					LcdPutChar8( (xx + j),yy+1,~font[m+j+8] );
				}
			}
		}
		else
			break;
			
		xx += 8;
		if( xx > 96 )
			return;
	}
	PutPic( (void *)LcdBuf );
}
//显示16*16字符
void LcdPutString16_16( u8 x,u8 y,u8 *ptr,u8 len,u8 op )
{
	u8 i,j,*tptr = ptr,xx = x,yy = y;
	u16 m;
	if( xx > 95)
		return ;
	if( yy )
		return ;	
	for( i = 0;i < len; i ++ )
	{
		m = lookforChar(*tptr ++);
		if( m != FONTLISTCOUNT )
			{
				m = m * 32;
				for( j = 0;j < 16;j ++ )
				{
					if(op)
					{
						LcdPutChar8( (xx + j),yy,font[m+j] );
						LcdPutChar8( (xx + j),yy+1,font[m+j+16] );
					}
					else
					{
						LcdPutChar8( (xx + j),yy,~font[m+j] );
						LcdPutChar8( (xx + j),yy+1,~font[m+j+16] );
					}
				}
			}
		else
			break;
		xx += 16;
		if( xx > 80 )
			return;
	}
	PutPic( (void *)LcdBuf );
}

static void LcdPrint8( u8 x,u8 y,u8 vl,u8 op )
{
	u8 j;
	u16 m;
	m = lookforChar( vl );
	if( m != FONTLISTCOUNT )
	{
			m = m * 16;
			for( j = 0;j < 8;j ++ )
				{
					if(op)
					{
						LcdPutChar8( (x + j),y,font[m+j] );
						LcdPutChar8( (x + j),y+1,font[m+j+8] );
					}
					else
					{
						LcdPutChar8( (x + j),y,~font[m+j] );
						LcdPutChar8( (x + j),y+1,~font[m+j+8] );
					}
				}
	}
}
static void LcdPrint16( u8 x, u8 y, u16 val, u8 op )
{
	u8 j;
	u16 m;
	m = lookforChar16( val );
	if( m != fontChar16ListCount )
		{
			m = m * 32;
			for( j = 0;j < 16;j ++ )
			{
				if(op)
				{
					LcdPutChar8( (x + j),y,fontChar16[m+j] );
					LcdPutChar8( (x + j),y+1,fontChar16[m+j+16] );
				}
				else
				{
					LcdPutChar8( (x + j),y,~fontChar16[m+j] );
					LcdPutChar8( (x + j),y+1,~fontChar16[m+j+16] );
				}
			}
		}	
}

void LcdPutDispBuf( u8 x,u8 y,OledCodeDataType *ptr,u8 op )
{
	u8 tcount = x;
	OledCodeDataType *tptr = ptr;
	u16 temp16;
	if( x > 88 )
		return ;
	if( y > 1 )
		return;
	while( (*tptr != '\0') && ( tcount <= 88) )
	{
		if(*tptr < 127)											//显示ASIC码
			{
				LcdPrint8( tcount,y,*tptr,op );				
				tptr ++;
				tcount += 8;
			}
		else																//显示汉字
			{
				temp16 = tptr[1];
				temp16 <<= 8;
				temp16 |= tptr[0];
				LcdPrint16( tcount,y,temp16,op );
				tptr += 2;
				tcount += 16;
			}
	}
	PutPic( (void *)LcdBuf );
}

//实现中英文混合显示
void LcdPutDisp( u8 x,u8 y,OledCodeDataType *ptr,u8 op )
{
	u8 tcount = x;
	OledCodeDataType *tptr = ptr;
	u16 temp16;
	if( x > 88 )
		return ;
	if( y > 1 )
		return;
	while( (*tptr != '\0') && ( tcount <= 88) )
	{
		if(*tptr < 127)											//显示ASIC码
			{
				LcdPrint8( tcount,y,*tptr,op );				
				tptr ++;
				tcount += 8;
			}
		else																//显示汉字
			{
				temp16 = tptr[1];
				temp16 <<= 8;
				temp16 |= tptr[0];
				LcdPrint16( tcount,y,temp16,op );
				tptr += 2;
				tcount += 16;
			}
	}
	PutPic( (void *)LcdBuf );
}
//从右往左输出一组字符并移运显示
void LcdPutScDispRtoL( OledCodeDataType *ptr,u8 op,u16 dl )
{
	OledCodeDataType *tptr = ptr;
	u16 temp16;
//	LcdClearRam();
	while( *tptr != '\0' )
	{
		if(*tptr < 127)											//显示ASIC码
			{
				memcpy(LcdBuf[0],&LcdBuf[0][8],88);
				memcpy(LcdBuf[1],&LcdBuf[1][8],88);
				LcdPrint8( 88,0,*tptr,op );				
				tptr ++;				
			}
		else																//显示汉字
			{
				memcpy(LcdBuf[0],&LcdBuf[0][16],80);
				memcpy(LcdBuf[1],&LcdBuf[1][16],80);
				temp16 = tptr[1];
				temp16 <<= 8;
				temp16 |= tptr[0];
				LcdPrint16( 80,0,temp16,op );
				tptr += 2;
			}
		PutPic( (void *)LcdBuf );
		LcdDelay( dl );
	}	
}

void LcdPutScDispRtoL12( OledCodeDataType *ptr,u8 op,u16 dl )
{
	OledCodeDataType *tptr = ptr;
	u16 i,temp16;
	for( i = 0;i < 12;)
	{
		if(*tptr < 127)											//显示ASIC码
			{
				memcpy(LcdBuf[0],&LcdBuf[0][8],88);
				memcpy(LcdBuf[1],&LcdBuf[1][8],88);
				LcdPrint8( 88,0,*tptr,op );				
				tptr ++;	
				i ++	;		
			}
		else																//显示汉字
			{
				memcpy(LcdBuf[0],&LcdBuf[0][16],80);
				memcpy(LcdBuf[1],&LcdBuf[1][16],80);
				temp16 = tptr[1];
				temp16 <<= 8;
				temp16 |= tptr[0];
				LcdPrint16( 80,0,temp16,op );
				tptr += 2;
				i +=2;
			}
		PutPic( (void *)LcdBuf );
		LcdDelay( dl );
	}	
}

//从左往右
void LcdPutScDispLtoR12( OledCodeDataType *ptr,u8 op,u16 dl )
{
	OledCodeDataType *ttptr,*tptr = ptr;
	u16 temp16;
	u8 i,len,tempbuf[2][96];	
	len = 12;	
	tptr = ptr+11;
	for( i = 0; i < len;  )
	{
		if( *(tptr)< 127 )											//显示ASIC码
			{
				memcpy(&tempbuf[0][0],&LcdBuf[0][0],96 );
				memcpy(&tempbuf[1][0],&LcdBuf[1][0],96 );
				
				memcpy(&LcdBuf[0][8],&tempbuf[0][0],88);
				memcpy(&LcdBuf[1][8],&tempbuf[1][0],88);
				LcdPrint8( 0,0,*tptr,op );				
				tptr --;	
				i ++;			
			}
		else																//显示汉字
			{
				memcpy(&tempbuf[0][0],&LcdBuf[0][0],96 );
				memcpy(&tempbuf[1][0],&LcdBuf[1][0],96 );
				memcpy(&LcdBuf[0][16],&tempbuf[0][0],80);
				memcpy(&LcdBuf[1][16],&tempbuf[1][0],80);
				ttptr = tptr;
				temp16 = *ttptr;
				temp16 <<= 8;
				ttptr = tptr-1;
				temp16 |= *ttptr;
				LcdPrint16( 0,0,temp16,op );
				tptr -= 2;
				i += 2;
			}
		PutPic( (void *)LcdBuf );
		LcdDelay( dl );
	}	
}
void LcdPutScString( OledCodeDataType *ptr,u8 op,u8 rl,u16 dl )
{	
	switch( rl )
	{
		case LIFT_SC:
			LcdPutScDispLtoR12( ptr,op,dl );
			break;
		case RIGHT_SC:
			LcdPutScDispRtoL12( ptr,op,dl );
			break;
		default:break;	
	}
}


void LcdPutPic( u8 x, u8 y,u8 w,u8 h,OledCodeDataType *ptr,u8 op )
{
	u8 i;
	OledCodeDataType *tptr = ptr;
	if( (x > 95) || ((x + w) > 96) )
		return;
	if( (y > 1) || ((y + h) > 2))
		return;
	
	for( i = 0;i < w; i ++ )	
	{
		if(op)
		{	
			LcdBuf[y][x + i] = *tptr ;
			if( h == 2 )
				LcdBuf[y+1][x + i] = *(tptr+w);
			tptr ++;
		}
		else
		{
			LcdBuf[y][x + i] = ~*tptr ;
			if( h == 2 )
				LcdBuf[y+1][x + i] = ~*(tptr+w);
			tptr ++;
		}
	}
	PutPic( (void *)LcdBuf );
}


//整屏滑动显示
void LcdPutScPic(  OledCodeDataType *ptr, u8 op,u8 qp,u16 dl )
{
	u8 i,j;
	u8 tempbuf[2][96];
	if(qp)
	{
		for( i = 0 ;i < 96; i ++ )
		{
			tempbuf[0][i] = *ptr++;
		}
		for( i = 0 ;i < 96; i ++ )
		{
			tempbuf[1][i] = *ptr++;
		}	
	}
	else
	{
		for( i = 0 ;i < 96; i ++ )
		{
			tempbuf[0][i] = ~*ptr++;
		}
		for( i = 0 ;i < 96; i ++ )
		{
			tempbuf[1][i] = ~*ptr++;
		}	
	}
	
	switch( op )
	{
		case RIGHT_SC:			//右边
			for( i = 0; i < 8; i ++ )
			{
				for(j = 0;j < 84; j ++ )
				{
					LcdBuf[0][95-j] = LcdBuf[0][83 - j];
					LcdBuf[1][95-j] = LcdBuf[1][83 - j];
				}
				for( j = 0;j < 12; j ++ )
				{
					LcdBuf[0][11-j] = tempbuf[0][95 - j];
					LcdBuf[1][11-j] = tempbuf[1][95 - j];
				}
				for(j = 0;j < 84; j ++ )
				{
					tempbuf[0][95-j] = tempbuf[0][83 - j];
					tempbuf[1][95-j] = tempbuf[1][83 - j];
				}
				PutPic( (void *)LcdBuf );				
			}
			LcdDelay( dl );
			break;
		case LIFT_SC: 			//左边
			for( i = 0; i < 8; i ++ )
			{
				for(j = 0;j < 84; j ++ )
				{
					LcdBuf[0][j] = LcdBuf[0][j + 12];
					LcdBuf[1][j] = LcdBuf[1][j + 12];
				}
				for( j = 0;j < 12; j ++ )
				{
					LcdBuf[0][95-j] = tempbuf[0][11-j];
					LcdBuf[1][95-j] = tempbuf[1][11-j];
				}
				for(j = 0;j < 84; j ++ )
				{
					tempbuf[0][j] = tempbuf[0][j+12];
					tempbuf[1][j] = tempbuf[1][j+12];
				}
				PutPic( (void *)LcdBuf );				
			}
			LcdDelay( dl );
			break;
		default:
			break;
	}	
}






