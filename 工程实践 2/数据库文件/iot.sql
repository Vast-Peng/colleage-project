-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: iot
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ip_log`
--

DROP TABLE IF EXISTS `ip_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_log` (
  `nation` char(20) DEFAULT NULL,
  `province` char(20) DEFAULT NULL,
  `city` char(20) DEFAULT NULL,
  `Device` char(20) DEFAULT NULL,
  `IP` char(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip_log`
--

LOCK TABLES `ip_log` WRITE;
/*!40000 ALTER TABLE `ip_log` DISABLE KEYS */;
INSERT INTO `ip_log` VALUES ('Chiness','sichuan','shuangliu','web','192.168.1.1'),('Chinese','sichuan','chengdu','mobile','192.168.1.2'),('Chinese','sichuan','meishan','android','192.168.1.3');
/*!40000 ALTER TABLE `ip_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_root`
--

DROP TABLE IF EXISTS `login_root`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_root` (
  `pwd` char(16) NOT NULL,
  `supper` bit(1) DEFAULT NULL,
  PRIMARY KEY (`pwd`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_root`
--

LOCK TABLES `login_root` WRITE;
/*!40000 ALTER TABLE `login_root` DISABLE KEYS */;
INSERT INTO `login_root` VALUES ('woaini00',''),('PQH286214a','\0');
/*!40000 ALTER TABLE `login_root` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `more_info`
--

DROP TABLE IF EXISTS `more_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `more_info` (
  `ID` char(20) DEFAULT NULL,
  `description` text,
  `time` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `more_info`
--

LOCK TABLES `more_info` WRITE;
/*!40000 ALTER TABLE `more_info` DISABLE KEYS */;
INSERT INTO `more_info` VALUES ('cuit_001','kitchen','0000-00-00 00:00:00'),('cuit_002','bashroom','0000-00-00 00:00:00'),('cuit_003','room','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `more_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor`
--

DROP TABLE IF EXISTS `sensor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensor` (
  `ID` char(20) DEFAULT NULL,
  `name` char(50) DEFAULT NULL,
  `function` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor`
--

LOCK TABLES `sensor` WRITE;
/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;
INSERT INTO `sensor` VALUES ('cuit_001','temperature','check the temperature'),('cuit_002','water','check the water'),('cuit_003','press','check the press');
/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor_bill`
--

DROP TABLE IF EXISTS `sensor_bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensor_bill` (
  `ID` char(20) DEFAULT NULL,
  `nuy_price` decimal(10,0) DEFAULT NULL,
  `buy_time` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor_bill`
--

LOCK TABLES `sensor_bill` WRITE;
/*!40000 ALTER TABLE `sensor_bill` DISABLE KEYS */;
INSERT INTO `sensor_bill` VALUES ('cuit_001','100','0000-00-00 00:00:00'),('cuit_002','80','0000-00-00 00:00:00'),('cuit_003','120','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sensor_bill` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-30 23:47:23
